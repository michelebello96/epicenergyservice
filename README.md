# EpicEnergyService

Sistema CRM per un'azienda fornitrice di energia!

## Dettagli Progetto [^1]
[^1]:*(Maggiori dettagli presenti direttamente nel codice)*

Occorre realizzare il backend di un sistema CRM per un'azienda fornitrice di energia, denominata "EPIC ENERGY 
SERVICES", che vuole gestire i contatti con i propri clienti business.

Il sistema, basato su Web Service REST Spring Boot e database PostgreSQL, deve permettere di gestire un elenco dei 

clienti, che sono caratterizzati dai seguenti dati:
- ragioneSociale
- partitaIva
- email
- dataInserimento
- dataUltimoContatto
- fatturatoAnnuale
- pec
- telefono
- emailContatto
- nomeContatto
- cognomeContatto
- telefonoContatto

Ogni cliente può avere fino a due indirizzi, uno per la sede legale ed uno per la sede operativa.

I comuni sono gestiti attraverso un'anagrafica centralizza e sono caratterizzati da un nome e da un riferimento ad una 

provincia, anch'essa gestita in anagrafica centralizzata e caratterizzata da un nome ed una sigla. 

I clienti possono essere di vario tipo:
- PA
- SAS
- SPA
- SRL

Associato ad ogni cliente c'è un insieme di fatture.

Ogni fattura ha uno stato. Gli stati fattura possono essere dinamici, in quanto in base all'evoluzione del business 
possono essere inseriti nel sistema nuovi stati.

Il back-end deve fornire al front-end tutte le funzioni necessarie a gestire in modo completo (Aggiungere, modificare 
ed eleiminare)i suddetti elementi.

Deve essere possibile ordinare i clienti.

Deve essere possibile filtrare la lista clienti.

Deve essere possibile filtrare le fatture.

Per gestire in modo efficiente un numero cospicuo di elementi, occorre utilizzare la paginazione.

Prevedere inoltre un sistema di autenticazione e autorizzazione basato su token JWT che permetta a diversi utenti di 
accedere alle funzioni del backend e di registrarsi al sistema.

Gli utenti possono essere di tipo USER, abilitato alle sole operazioni di lettura, oppure ADMIN, abilitato a tutte le 
operazioni. Un utente può avere più ruoli.

* Importazione Comuni e Province
Viene fornito un elenco dei comuni in formato CSV, che deve essere importato nel sistema per mezzo di una appositoa 
procedura Java da eseguire manualmente per popolare il db.

Contestualmente alla realizzazione del sistema occorre inoltre:
- Realizzare una collection Postman contenente tutte le chiamate alle API del servizio, comprese quelle di 
autenticazione
- Implementare i principali test con JUnit


****** EXTRA ******


1. Realizzare un piccolo frontend per il nostro applicativo/api di backend, sfruttando le tecnologie Thymeleaf. 
Realizzare un piccolo portalino con delle pagine che permettano l'accesso alle funzioni CRUD 
e di ricerca sulle entità Cliente, Fattura, Utente, Comune, Provincia.


2. realizzare un piccolo servizio separato, secondo l'architettura MicroServices,
che interroghi parte delle API presenti nel resto dell'applicazione, 
per la realizzazione di statistiche sui clienti, basate sulla categoria di fatturato 
e distribuzione sul territorio nazionale (provincia/regione). A sua volta, questo 
servizio secondario dovrà essere richiamato dal client costruito al punto 1. 
Per la realizzazione di una "dashboard" di visualizzazione e distribuzione clienti.

