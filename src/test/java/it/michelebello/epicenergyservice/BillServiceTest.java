package it.michelebello.epicenergyservice;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import it.michelebello.epicenergyservice.models.data.Bill;
import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.services.BillService;
import it.michelebello.epicenergyservice.services.BillingStatusService;
import it.michelebello.epicenergyservice.services.CustomerService;
import it.michelebello.epicenergyservice.services.TypeService;


@SpringBootTest
class BillServiceTest {
	@Autowired
	BillService bills;
	@Autowired
	CustomerService customers;
	@Autowired
	BillingStatusService statuses;
	@Autowired
	TypeService types;
	
	@Test
	void testGetTotalRevenue() {
		var b = bills.getByCustomer(customers.getById(1), null).getContent();
		double sum = 0;
		for(Bill x: b)
			sum += x.getAmount().doubleValue();
		
		assertEquals(sum, bills.getTotalRevenue(1), "The method must return the same amount as sum");
	}

	@Test
	void testDeleteByCustomer() {
		bills.save(Bill.builder()
				.withCustomer(customers.getById(1))
				.withAmount(BigDecimal.valueOf(1234))
				.withBillingNumber(5)
				.withStatus(statuses.getById(3))
				.withBillingDate(LocalDate.of(2021, 9, 26))
				.build());
		
		assertNotEquals(0, bills.deleteByCustomer(customers.getById(1)));
	}

}
