package it.michelebello.epicenergyservice;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import it.michelebello.epicenergyservice.models.data.Contact;
import it.michelebello.epicenergyservice.services.ContactService;
import it.michelebello.epicenergyservice.services.CustomerService;
import it.michelebello.epicenergyservice.services.TypeService;

@SpringBootTest
class ContactServiceTest {

	@Autowired
	ContactService contacts;
	@Autowired
	CustomerService customers;
	@Autowired
	TypeService types;
	
	@Test
	void testGetByEmail() {
		var c = Contact.builder()
				.withCustomer(customers.getById(1))
				.withEmail("TestTEST")
				.withName("Test2")
				.withSurname("Test2")
				.withPhoneNumber("testTest")
				.build();
		contacts.save(c);
		
		assertEquals("TestTEST", contacts.getByEmail("TestTEST").getEmail() ,"The strings must be equal");
		contacts.delete(c.getId());
	}

}
