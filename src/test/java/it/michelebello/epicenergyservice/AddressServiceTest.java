package it.michelebello.epicenergyservice;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import it.michelebello.epicenergyservice.citiesloader.services.CitiesService;
import it.michelebello.epicenergyservice.models.data.Address;
import it.michelebello.epicenergyservice.models.enums.BuildingUse;
import it.michelebello.epicenergyservice.services.AddressService;
import it.michelebello.epicenergyservice.services.CustomerService;

@SpringBootTest
class AddressServiceTest {

	@Autowired
	AddressService services;
	@Autowired
	CitiesService cities;
	@Autowired
	CustomerService customers;
	
	@Test
	void testUpdate() {
		var x = services.getById(1);
		x.setHouseNumber(1234);
		services.update(x);
		assertEquals(1234, services.getById(1).getHouseNumber(),"The method must return the exact integer of the test");	
	}

	@Test
	void testGetByCity() {
		var x = Address.builder()
				.withCity(cities.getByName("Cervinara", "AV").get())
				.withCustomer(customers.getById(1))
				.withHouseNumber(123)
				.withPostalCode("00123")
				.withStreet("via Test")
				.withUse(BuildingUse.WORKPLACE)
				.build();
		services.save(x);
		
		assertNotEquals(0,services.getByCity(cities.getByName("Cervinara", "AV").get(), null).getContent().size(), "The method must return a Page with more than 0 elements");
		services.delete(x.getId());
	}

}
