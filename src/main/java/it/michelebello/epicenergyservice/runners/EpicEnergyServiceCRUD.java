package it.michelebello.epicenergyservice.runners;

import javax.annotation.Priority;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import it.michelebello.epicenergyservice.models.data.BillingStatus;
import it.michelebello.epicenergyservice.models.data.Role;
import it.michelebello.epicenergyservice.models.data.Type;
import it.michelebello.epicenergyservice.models.enums.RoleType;
import it.michelebello.epicenergyservice.services.BillingStatusService;
import it.michelebello.epicenergyservice.services.RoleService;
import it.michelebello.epicenergyservice.services.TypeService;

/*
 * Questo runner che viene eseguito dopo quello relativo alle città
 * permette di popolare il database con i principali tipi di società,
 * con gli stati di fatturazione e con i ruoli presenti nel servizio.
 */
@Component
@Priority(2)
public class EpicEnergyServiceCRUD implements CommandLineRunner{

	@Autowired
	TypeService types;
	@Autowired
	BillingStatusService statuses;
	@Autowired
	RoleService roles;
	
	@Override
	public void run(String... args) throws Exception {
		
//		//Creo i tipi di società e popolo la rispettiva tabella
//		
//		Type t1 = new Type();
//		t1.setType("S.S.");
//		Type t2 = new Type();
//		t2.setType("S.N.C.");
//		Type t3 = new Type();
//		t3.setType("S.A.S.");
//		Type t4 = new Type();
//		t4.setType("SOCIETÀ DI CAPITALI");
//		Type t5 = new Type();
//		t5.setType("S.R.L.");
//		Type t6 = new Type();
//		t6.setType("S.P.A.");
//		Type t7 = new Type();
//		t7.setType("S.A.P.A.");
//		Type t8 = new Type();
//		t8.setType("SOCIETÀ COOPERATIVE");
//		
//		types.save(t1);
//		types.save(t2);
//		types.save(t3);
//		types.save(t4);
//		types.save(t5);
//		types.save(t6);
//		types.save(t7);
//		types.save(t8);
//		/*******************************************************************************/
//		
//		//Creo i gli stati della fattura e popolo la rispettiva tabella
//		
//		BillingStatus bs1 = new BillingStatus();
//		bs1.setStatus("IN_BOZZA");
//		BillingStatus bs2 = new BillingStatus();
//		bs2.setStatus("IN_ELABORAZIONE");
//		BillingStatus bs3 = new BillingStatus();
//		bs3.setStatus("INVIATA_AL_SDI");
//		BillingStatus bs4 = new BillingStatus();
//		bs4.setStatus("SCARTATA");
//		BillingStatus bs5 = new BillingStatus();
//		bs5.setStatus("CONSEGNATA");
//		BillingStatus bs6 = new BillingStatus();
//		bs6.setStatus("NON_CONSEGNATA");
//		
//		statuses.save(bs1);
//		statuses.save(bs2);
//		statuses.save(bs3);
//		statuses.save(bs4);
//		statuses.save(bs5);
//		statuses.save(bs6);
//		/********************************************************************************/
//		
//		//Creo i ruoli possibili e popolo la rispettiva tabella
//		
//		Role r1 = new Role();
//		r1.setRoleType(RoleType.ROLE_ADMIN);
//		Role r2 = new Role();
//		r2.setRoleType(RoleType.ROLE_USER);
//		
//		roles.save(r1);
//		roles.save(r2);
	
	}

}
