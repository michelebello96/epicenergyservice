package it.michelebello.epicenergyservice.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.citiesloader.models.City;
import it.michelebello.epicenergyservice.models.data.Address;
import it.michelebello.epicenergyservice.models.data.Customer;

//Interfaccia dei servizi per gli indirizzi

public interface AddressService {

	Address save(Address address);
	Address update(Address address);
	Address delete(int id);
	Address getById(int id);
	Address getByCityAndStreetAndHouseNumber(City city, String street, int houseNumber);
	Page<Address> getByCustomer(Customer customer, Pageable pageable);
	Page<Address> getByCity(City city, Pageable pageable);
	Page<Address> getByPostalCode(String PostalCode, Pageable pageable);
	Page<Address> getAll(Pageable pageable);
	List<Address> getAll();
	int deleteByCustomer(Customer customer);
}
