package it.michelebello.epicenergyservice.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.models.data.User;

//Interfaccia dei servizi per gli utenti

public interface UserService {

	User save(User user);
	User update(User user);
	User delete(int id);
	User getById(int id);
	Optional<User> findByUsername(String username);
	Page<User> getAll(Pageable pageable);
	
}
