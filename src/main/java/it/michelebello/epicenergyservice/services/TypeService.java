package it.michelebello.epicenergyservice.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.models.data.Type;

//Interfaccia dei servizi per i tipi di società

public interface TypeService {
	
	Type save(Type type);
	Type update(Type type);
	Type delete(int id);
	Type getByType(String type);
	Type getById(int id);
	Page<Type> getAll(Pageable pageable);
}
