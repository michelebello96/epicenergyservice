package it.michelebello.epicenergyservice.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.models.data.Customer;

//Interfaccia del servizio di ordinamento dei clienti

public interface CustomerSorterService {

	Page<Customer> getAllSorted(Pageable pageable, int sorter);
}
