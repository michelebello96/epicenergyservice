package it.michelebello.epicenergyservice.services;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.models.data.Customer;

//Interfaccia dei servizi per i clienti

public interface CustomerService {

	Customer save(Customer customer);
	Customer update(Customer customer);
	Customer delete(int id);
	Customer getById(int id);
	Page<Customer> getByAnnualRevenueRange(BigDecimal fromAmount, BigDecimal toAmount, Pageable page);
	Page<Customer> getByCreationDateRange(LocalDate fromDate, LocalDate toDate, Pageable page);
	Page<Customer> getByLastContactRange(LocalDate fromDate, LocalDate toDate, Pageable page);
	Page<Customer> getByNameContain(String name, Pageable page);
	List<Customer> getAll();
	
}
