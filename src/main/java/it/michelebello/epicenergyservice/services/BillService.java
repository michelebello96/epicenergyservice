package it.michelebello.epicenergyservice.services;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.models.data.Bill;
import it.michelebello.epicenergyservice.models.data.BillingStatus;
import it.michelebello.epicenergyservice.models.data.Customer;

//Interfaccia dei servizi per le fatture

public interface BillService {

	Bill save(Bill bill);
	Bill update(Bill bill);
	Bill delete(int id);
	Bill getById(int id);
	Page<Bill> getBetweenDates(LocalDate d1, LocalDate d2, Pageable pageable);
	Page<Bill> getByYear(int year, Pageable pageable);
	Page<Bill> getByAmountRange(BigDecimal min,BigDecimal max, Pageable pageable);
	Page<Bill> getByStatus(BillingStatus status, Pageable pageable);
	Page<Bill> getByCustomer(Customer customer, Pageable pageable);
	Page<Bill> getAll(Pageable pageable);
	List<Bill> getAll();
	List<String> getAllAmountsPerYear(int year);
	BigDecimal getBillingAmountPerYear(int idCustomer, int year);
	BigDecimal getTotalRevenue(int idCustomer);
	int deleteByCustomer(Customer customer);
	
}
