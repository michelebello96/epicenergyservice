package it.michelebello.epicenergyservice.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.models.data.BillingStatus;

//Interfaccia dei servizi per gli stati di fatturazione

public interface BillingStatusService {

	BillingStatus save(BillingStatus status);
	BillingStatus update(BillingStatus status);
	BillingStatus delete(int id);
	BillingStatus getByStatus(String status);
	BillingStatus getById(int id);
	Page<BillingStatus> getAll(Pageable pageable);
	
}
