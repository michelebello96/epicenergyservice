package it.michelebello.epicenergyservice.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.models.data.Contact;
import it.michelebello.epicenergyservice.models.data.Customer;

//Interfaccia dei servizi per i contatti

public interface ContactService {

	Contact save(Contact contact);
	Contact update(Contact contact);
	Contact delete(int id);
	Contact getByEmail(String email);
	Contact getByPhoneNumber(String phoneNumber);
	Page<Contact> getByName(String name, Pageable pageable);
	Page<Contact> getBySurmane(String surname, Pageable pageable);
	Page<Contact> getByCustomer(Customer customer, Pageable pageable);
	Page<Contact> getAll(Pageable pageable);
	int deleteByCustomer(Customer customer);
	
}
