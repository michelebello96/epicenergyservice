package it.michelebello.epicenergyservice.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.models.data.Role;
import it.michelebello.epicenergyservice.models.enums.RoleType;

//Interfaccia dei servizi per i ruoli degli utenti

public interface RoleService {

	Role save(Role role);
	Role update(Role role);
	Role delete(int id);
	Role getById(int id);
	Role getByRoleType(RoleType roletype);
	Page<Role> getAll(Pageable pageable);
	
}
