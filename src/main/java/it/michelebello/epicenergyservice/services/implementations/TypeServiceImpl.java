package it.michelebello.epicenergyservice.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.michelebello.epicenergyservice.models.data.Type;
import it.michelebello.epicenergyservice.repositories.TypeRepository;
import it.michelebello.epicenergyservice.services.TypeService;

@Service
public class TypeServiceImpl implements TypeService{

	@Autowired
	TypeRepository types;
	
	//Sono qui presenti le implementazioni dei servizi dei tipi di società

	@Override//metodo per l'aggiunta in DB
	public Type save(Type type) {
		try {
			return types.save(type);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo per la modifica
	public Type update(Type type) {
		try {
			var t = types.findById(type.getId()).get();
			if(type.getType()!=null)
				if(!type.getType().isBlank())
					t.setType(type.getType());
			return types.save(t);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo per la rimozione dal DB
	public Type delete(int id) {
		try {
			var t = types.findById(id).get();
			types.delete(t);
			return t;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	//Seguono vari metodi per il recupero dal DB
	@Override
	public Type getByType(String type) {
		try {
			return types.findByType(type).get();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Type getById(int id) {
		try {
			return types.findById(id).get();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Type> getAll(Pageable pageable) {
		try {
			return types.findAll(pageable);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

}
