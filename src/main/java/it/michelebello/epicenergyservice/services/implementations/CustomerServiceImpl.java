package it.michelebello.epicenergyservice.services.implementations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.repositories.CustomerRepository;
import it.michelebello.epicenergyservice.services.AddressService;
import it.michelebello.epicenergyservice.services.BillService;
import it.michelebello.epicenergyservice.services.ContactService;
import it.michelebello.epicenergyservice.services.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{

	@Autowired
	CustomerRepository customers;
	@Autowired
	BillService bills;
	@Autowired
	AddressService addresses;
	@Autowired
	ContactService contacts;
	
	//Sono qui presenti le implementazioni dei servizi dei clienti
	
	@Override//metodo per l'aggiunta in DB
	public Customer save(Customer customer) {
		try {
			customer.setLastContact(LocalDate.now());
			return customers.save(customer);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo per la modifica (Sono presenti molti controlli per garantire che solo i campi inseriti dall'utente rientrino nelle modifiche)
	public Customer update(Customer customer) {
		try {
			var modify = customers.findById(customer.getId()).get();
			
			if(customer.getAnnualRevenue() != null)
				modify.setAnnualRevenue(customer.getAnnualRevenue());
			if(customer.getBusinessEmail() != null)
				if(!customer.getBusinessEmail().isBlank())
					modify.setBusinessEmail(customer.getBusinessEmail());
			if(customer.getBusinessName() != null)
				if(!customer.getBusinessName().isBlank())
					modify.setBusinessName(customer.getBusinessName());
			if(customer.getPec() != null)
				if(!customer.getPec().isBlank())
					modify.setPec(customer.getPec());
			if(customer.getPhoneNumber()!= null)
				if(!customer.getPhoneNumber().isBlank())
					modify.setPhoneNumber(customer.getPhoneNumber());
			if(customer.getType()!= null)
				modify.setType(customer.getType());
			if(customer.getVatNumber()!= null)
				if(!customer.getVatNumber().isBlank())
					modify.setVatNumber(customer.getVatNumber());
			
			return customers.save(modify);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo di rimozione dal BD
	public Customer delete(int id) {
		try {
			var c = customers.findById(id).get();
			contacts.deleteByCustomer(c);
			bills.deleteByCustomer(c);
			addresses.deleteByCustomer(c);
			customers.delete(c);
			return c;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	//Seguono vari metodi per il recupero dal DB
	@Override
	public Customer getById(int id) {
		try {
			return customers.findById(id).get();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Customer> getByCreationDateRange(LocalDate fromDate, LocalDate toDate, Pageable pageable) {
		try {
			//E' presente un controllo che garantisce il funzionamento del metodo anche in caso di inserimento invertito dall'utente
			if(toDate.isAfter(fromDate)) {
				return customers.findByCreationDateAfterAndCreationDateBefore(fromDate.minusDays(1), toDate.plusDays(1), pageable);
			}else {
				return customers.findByCreationDateAfterAndCreationDateBefore(toDate.minusDays(1), fromDate.plusDays(1), pageable);
			}
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Customer> getByLastContactRange(LocalDate fromDate, LocalDate toDate, Pageable page) {
		try {
			//E' presente un controllo che garantisce il funzionamento del metodo anche in caso di inserimento invertito dall'utente
			if(toDate.isAfter(fromDate)) {
				return customers.findByLastContactAfterAndLastContactBefore(fromDate.minusDays(1), toDate.plusDays(1), page);
			}else {
				return customers.findByLastContactAfterAndLastContactBefore(toDate.minusDays(1), fromDate.plusDays(1), page);
			}
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Customer> getByNameContain(String name, Pageable page) {
		try {
			return customers.findByBusinessNameContainingIgnoreCase(name, page);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Customer> getByAnnualRevenueRange(BigDecimal fromAmount, BigDecimal toAmount, Pageable page) {
		try {
			//E' presente un controllo che garantisce il funzionamento del metodo anche in caso di inserimento invertito dall'utente	
			if(fromAmount.compareTo(toAmount) <= 0) {
				var lista = customers.findAllByOrderByIdAsc();						// Questo metodo esegue uno stream della lista di clienti filtrando solo quelli che 
				List<Customer> target = new ArrayList<Customer>();		// che rientrano dei limiti richiesti
				target = lista.stream()
					.filter(c -> c.getAnnualRevenue().compareTo(toAmount) <= 0 && c.getAnnualRevenue().compareTo(fromAmount) >= 0)
					.toList();
				
				return new PageImpl<Customer>(target,page, target.size());
			}else {
				var lista = customers.findAllByOrderByIdAsc();
				List<Customer> target = new ArrayList<Customer>();
				target = lista.stream()
					.filter(c -> c.getAnnualRevenue().compareTo(toAmount)>= 0 && c.getAnnualRevenue().compareTo(fromAmount) <= 0)
					.toList();
				
				return new PageImpl<Customer>(target,page, target.size());
			}
				
			
			}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Customer> getAll() {
		try {
			return customers.findAll();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
}
