package it.michelebello.epicenergyservice.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.michelebello.epicenergyservice.models.data.BillingStatus;
import it.michelebello.epicenergyservice.repositories.BillingStatusRepository;
import it.michelebello.epicenergyservice.services.BillingStatusService;

@Service
public class BillingStatusServiceImpl implements BillingStatusService{

	@Autowired
	BillingStatusRepository statuses;
	
	//Sono qui presenti le implementazioni dei servizi dello statto delle fatture
	
	@Override//metodo per l'aggiunta in DB
	public BillingStatus save(BillingStatus status) {
		try {
			return statuses.save(status);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo per la modifica
	public BillingStatus update(BillingStatus status) {
		try {
			var s = statuses.findById(status.getId()).get();
			if(status.getStatus()!=null)
				if(!status.getStatus().isBlank())
					s.setStatus(status.getStatus());
			return statuses.save(s);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}

	@Override//metodo di rimozione dal BD
	public BillingStatus delete(int id) {
		try {
			var s = statuses.findById(id).get();
			statuses.delete(s);
			return s;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	//Seguono vari metodi per il recupero dal DB
	@Override
	public BillingStatus getByStatus(String status) {
		try {
			return statuses.findByStatusIgnoreCase(status).get();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public BillingStatus getById(int id) {
		try {
			return statuses.findById(id).get();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}

	@Override
	public Page<BillingStatus> getAll(Pageable pageable) {
		try {
			return statuses.findAll(pageable);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}
}
