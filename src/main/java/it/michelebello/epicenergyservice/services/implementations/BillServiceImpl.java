package it.michelebello.epicenergyservice.services.implementations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.michelebello.epicenergyservice.models.data.Address;
import it.michelebello.epicenergyservice.models.data.Bill;
import it.michelebello.epicenergyservice.models.data.BillingStatus;
import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.repositories.BillRepository;
import it.michelebello.epicenergyservice.repositories.CustomerRepository;
import it.michelebello.epicenergyservice.services.BillService;

@Service
public class BillServiceImpl implements BillService{

	@Autowired
	BillRepository bills;
	@Autowired
	CustomerRepository customers;
	
	//Sono qui presenti le implementazioni dei servizi delle fatture
	
	@Override//metodo per l'aggiunta in DB
	public Bill save(Bill bill) {
		try {
			if(bill.getCustomer()!=null) {	//Qualora la fattura inserita è più recente dell'ultimo contatto con il cliente, questo viene reimpostato alla data di fatturazione
				var c = customers.findById(bill.getCustomer().getId()).get();
				if(bill.getBillingDate().isAfter(c.getLastContact())) {
					c.setLastContact(bill.getBillingDate());
					customers.save(c);
				}
			}
			return bills.save(bill);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo per la modifica (Sono presenti molti controlli per garantire che solo i campi inseriti dall'utente rientrino nelle modifiche)
	public Bill update(Bill bill) {
		try {
			var b = bills.findById(bill.getId()).get();
			if(bill.getAmount()!=null)
				b.setAmount(bill.getAmount());
			if(bill.getBillingDate()!=null) {
				b.setBillingDate(bill.getBillingDate());
				if(bill.getCustomer()!=null) {  //Anche qui è presente il controllo sulla data dell'ultimo contatto
					var c = customers.findById(bill.getCustomer().getId()).get();
					if(bill.getBillingDate().isAfter(c.getLastContact())) {
						c.setLastContact(bill.getBillingDate());
						customers.save(c);
					}
				}
			}
			if(bill.getBillingNumber()!=null)
				b.setBillingNumber(bill.getBillingNumber());
			if(bill.getCustomer()!=null)
				b.setCustomer(bill.getCustomer());
			if(bill.getStatus()!=null)
				b.setStatus(bill.getStatus());
			
			return bills.save(b);	
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}

	@Override//metodo di rimozione dal BD
	public Bill delete(int id) {
		try {
			var b = bills.findById(id).get();
			bills.delete(b);
			return b;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}

	//Seguono vari metodi per il recupero dal DB
	@Override
	public Bill getById(int id) {
		try {
			return bills.findById(id).get();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}

	@Override
	public Page<Bill> getBetweenDates(LocalDate d1, LocalDate d2, Pageable pageable) {
		try {
			if(d1.isBefore(d2))
				return bills.findByBillingDateAfterAndBillingDateBefore(d1.minusDays(1), d2.plusDays(1), pageable);
			else
				return bills.findByBillingDateAfterAndBillingDateBefore(d2.minusDays(1), d1.plusDays(1), pageable);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Bill> getByYear(int year, Pageable pageable) {
		try {
			var list = bills.findAll();
			list = list.stream()
					.filter(b -> b.getBillingDate().getYear() == year)
					.toList();
			return new PageImpl<Bill>(list, pageable, list.size());
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Bill> getByAmountRange(BigDecimal min, BigDecimal max, Pageable pageable) {
		try {
			var list = bills.findAll();
			list = list.stream()
					.filter(b -> (b.getAmount().compareTo(min) > 0 && b.getAmount().compareTo(max) <= 0))
					.toList();
			return new PageImpl<Bill>(list, pageable, list.size());
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Bill> getByStatus(BillingStatus status, Pageable pageable) {
		try {
			return bills.findByStatus(status, pageable);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Bill> getByCustomer(Customer customer, Pageable pageable) {
		try {
			return bills.findByCustomer(customer, pageable);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}

	@Override
	public Page<Bill> getAll(Pageable pageable) {
		try {
			return bills.findAll(pageable);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}

	//Seguono tre metodi per il calcolo del fatturato 
	
	@Override
	public BigDecimal getTotalRevenue(int customer) {
		try {
			var list = bills.findByCustomer(customers.findById(customer).get(), null).getContent();
			var annualRevenue = list.stream()
					.map(n -> n.getAmount().doubleValue())
					.reduce(0.0,(sum, num)->sum+num);
			return BigDecimal.valueOf(annualRevenue);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public BigDecimal getBillingAmountPerYear(int idCustomer, int year) {
		try {
			var b = bills.findByCustomer(customers.findById(idCustomer).get(), null).getContent();
			
			var somma = b.stream().filter(n -> n.getBillingDate().getYear() == year)
				.map(n -> n.getAmount().doubleValue())
				.reduce(0.0,(sum, num)->sum+num);
			
			return BigDecimal.valueOf(somma);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<String> getAllAmountsPerYear(int year) {
		try {
			Map<Customer, Double> m = new HashMap<Customer, Double>();
			var bi = bills.findByBillingDateAfterAndBillingDateBefore(LocalDate.of(year, 1, 1), LocalDate.of(year, 12, 31), null).getContent();
			for(Bill b : bi) {
				if(m.containsKey(b.getCustomer())) {
					m.replace(b.getCustomer(), m.get(b.getCustomer()) + b.getAmount().doubleValue());
				}else {
					m.put(b.getCustomer(), b.getAmount().doubleValue());
				}
			}
			
			List<String> lista = new LinkedList<>();
			for(Customer c :m.keySet()) {
				lista.add(c.getBusinessName()+"->"+m.get(c));
			}
			
			return lista;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	//Questo metodo viene richiamato nei servizi del cliente, nello specifico l'eliminazione di un cliente 
	//viene preceduta, qualora ci siano, dall'eliminazione di tutte le fatture a lui associate		
	@Override
	public int deleteByCustomer(Customer customer) {
		try {
			var x = bills.findByCustomer(customer, null).getContent();
			int eliminati = 0;
			
			for(Bill b : x) {
				bills.delete(b);
				eliminati++;
			}
			
			return eliminati;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Bill> getAll() {
		try {
			return bills.findAll();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
}
