package it.michelebello.epicenergyservice.services.implementations;

import java.util.HashSet;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import it.michelebello.epicenergyservice.models.data.Role;
import it.michelebello.epicenergyservice.models.data.User;
import it.michelebello.epicenergyservice.models.enums.RoleType;
import it.michelebello.epicenergyservice.repositories.UserRepository;
import it.michelebello.epicenergyservice.services.RoleService;
import it.michelebello.epicenergyservice.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository users;
	@Autowired
	PasswordEncoder encoder;
	@Autowired
	RoleService roles;

	//Sono qui presenti le implementazioni dei servizi degli utenti

	@Override//metodo per l'aggiunta in DB
	public User save(User user) {
		try {
			user.setPassword(encoder.encode(user.getPassword()));
			if (user.getRoles() == null)
				user.setRoles(new HashSet<Role>());
			user.getRoles().add(roles.getByRoleType(RoleType.ROLE_USER));
			return users.save(user);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo per la modifica (Sono presenti molti controlli per garantire che solo i campi inseriti dall'utente rientrino nelle modifiche)
	public User update(User user) {
		try {
			var u = users.findById(user.getId()).get();
			if (user.getEmail() != null)
				if (!user.getEmail().isBlank())
					u.setEmail(user.getEmail());
			if (user.getName() != null)
				if (!user.getName().isBlank())
					u.setName(user.getName());
			if (user.getSurname() != null)
				if (!user.getSurname().isBlank())
					u.setSurname(user.getSurname());
			if (user.getUsername() != null)
				if (!user.getUsername().isBlank())
					u.setUsername(user.getUsername());
			if (user.getPassword() != null)
				if (!user.getPassword().isBlank())
					u.setPassword(encoder.encode(user.getPassword()));
			if (user.getRoles() != null)
				for (Role rol : user.getRoles())
					u.getRoles().add(rol);

			return users.save(u);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo di rimozione dal BD
	public User delete(int id) {
		try {
			var u = users.findById(id).get();
			users.delete(u);
			return u;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	//Seguono vari metodi per il recupero dal DB
	@Override
	public User getById(int id) {
		try {
			return users.findById(id).get();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<User> getAll(Pageable pageable) {
		try {
			return users.findAll(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Optional<User> findByUsername(String username) {
		try {
			return users.findByUsername(username);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

}
