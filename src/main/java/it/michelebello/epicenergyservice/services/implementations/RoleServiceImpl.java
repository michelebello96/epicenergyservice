package it.michelebello.epicenergyservice.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.michelebello.epicenergyservice.models.data.Role;
import it.michelebello.epicenergyservice.models.enums.RoleType;
import it.michelebello.epicenergyservice.repositories.RoleRepository;
import it.michelebello.epicenergyservice.services.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleRepository roles;

	//Sono qui presenti le implementazioni dei servizi dei ruoli degli utenti
	
	@Override//metodo per l'aggiunta in DB
	public Role save(Role role) {
		try {
			return roles.save(role);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo per la modifica 
	public Role update(Role role) {
		try {
			var r = roles.findById(role.getId()).get();
			if(role.getRoleType()!=null)
				r.setRoleType(role.getRoleType());
			
			roles.save(r);
			 
			return r;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo di rimozione dal BD
	public Role delete(int id) {
		try {
			var r = roles.findById(id).get();
			roles.delete(r);
			return r;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	//Seguono vari metodi per il recupero dal DB
	@Override
	public Role getById(int id) {
		try {
			return roles.findById(id).get();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Role getByRoleType(RoleType roletype) {
		try {
			return roles.findByRoleType(roletype).get();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}	}

	@Override
	public Page<Role> getAll(Pageable pageable) {
		try {
			return roles.findAll(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
