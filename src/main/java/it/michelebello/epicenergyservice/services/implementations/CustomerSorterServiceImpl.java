package it.michelebello.epicenergyservice.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.repositories.AddressRepository;
import it.michelebello.epicenergyservice.repositories.CustomerRepository;
import it.michelebello.epicenergyservice.services.CustomerSorterService;
import it.michelebello.epicenergyservice.sorter.CustomerSorter;
import it.michelebello.epicenergyservice.sorter.extensions.SorterByAnnualRavenue;
import it.michelebello.epicenergyservice.sorter.extensions.SorterByCreationDate;
import it.michelebello.epicenergyservice.sorter.extensions.SorterByLastContact;
import it.michelebello.epicenergyservice.sorter.extensions.SorterByName;
import it.michelebello.epicenergyservice.sorter.extensions.SorterByProvince;

@Service
public class CustomerSorterServiceImpl implements CustomerSorterService{

	@Autowired
	CustomerRepository customers;
	@Autowired
	AddressRepository addresses;
	
	// E' qui presente l'implementazione del servizio di sorting dei clienti
	// Ho scelto di gestire tale servizio come una catena di responsabilità 
	// ai cui anelli corrisponde un diverso metodo di sorting secondo tale ordine
	//	*	1 -> Nome
	//	*	2 -> Fattuarto annuo
	//	*	3 -> Data di creazione
	//	*	4 -> Data ultimo contatto
	//	*	5 -> Provincia della sede legale
	
	@Override
	public Page<Customer> getAllSorted(Pageable pageable, int sorter) {
		try {
			CustomerSorter c1 = new SorterByName(null);
			CustomerSorter c2 = new SorterByAnnualRavenue(c1);
			CustomerSorter c3 = new SorterByCreationDate(c2);
			CustomerSorter c4 = new SorterByLastContact(c3);
			CustomerSorter c5 = new SorterByProvince(c4, addresses);
			
			return c5.trySort(sorter, pageable, customers);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
}
