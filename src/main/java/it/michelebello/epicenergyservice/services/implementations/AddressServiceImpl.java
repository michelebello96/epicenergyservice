package it.michelebello.epicenergyservice.services.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.michelebello.epicenergyservice.citiesloader.models.City;
import it.michelebello.epicenergyservice.models.data.Address;
import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.models.enums.BuildingUse;
import it.michelebello.epicenergyservice.repositories.AddressRepository;
import it.michelebello.epicenergyservice.services.AddressService;
import it.michelebello.epicenergyservice.servicestatistics.error.StatisticException;

@Service
public class AddressServiceImpl implements AddressService{

	@Autowired
	AddressRepository addresses;
	
	//Sono qui presenti le implementazioni dei servizi degli indirizzi
	
	@Override//metodo per l'aggiunta in DB
	public Address save(Address address) {
		try {				//E' qui presente un controllo che verifica in caso di inserimento di sede legale che il cliente non ne abbia già una
			if(address.getUse()==BuildingUse.REGISTERED_OFFICE) {
				var check = addresses.findByCustomerAndUse(address.getCustomer(), BuildingUse.REGISTERED_OFFICE);
				if(check.isPresent()) {			//In questo caso l'indirizzo inserito verrà impostato a sede operativa automaticamente data
					address.setUse(BuildingUse.WORKPLACE);		//l'unicità delle sedi
					var check2 = addresses.findByCustomerAndUse(address.getCustomer(), BuildingUse.WORKPLACE);
					if(check2.isPresent())
						throw new RuntimeException("This customer already have 2 addresses");
				}
			}else {
				var check2 = addresses.findByCustomerAndUse(address.getCustomer(), BuildingUse.WORKPLACE);
				if(check2.isPresent()) {
					address.setUse(BuildingUse.REGISTERED_OFFICE);
					var check = addresses.findByCustomerAndUse(address.getCustomer(), BuildingUse.REGISTERED_OFFICE);
					if(check.isPresent())
						throw new RuntimeException("This customer already have 2 addresses");
				}
			}
			return addresses.save(address);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo per la modifica (Sono presenti molti controlli per garantire che solo i campi inseriti dall'utente rientrino nelle modifiche)
	public Address update(Address address) {
		try {
			var ad = addresses.findById(address.getId()).get();
			if(address.getCustomer() != null)
				if(!ad.getCustomer().equals(address.getCustomer()))	//in caso di inserimento di Customer divero ho scelto di mandare in errore per evitare
					throw new RuntimeException("Different customers insert");		//problemi di sovrapposizione di più sedi
			if(address.getCity() != null)
				ad.setCity(address.getCity());
			if(address.getHouseNumber() != null)
				ad.setHouseNumber(address.getHouseNumber());
			if(address.getLocality() != null)
				if(!address.getLocality().isBlank())
					ad.setLocality(address.getLocality());
			if(address.getPostalCode()!=null)
				if(!address.getPostalCode().isBlank())
					ad.setPostalCode(address.getPostalCode());
			if(address.getStreet()!=null)
				if(!address.getStreet().isBlank())
					ad.setStreet(address.getStreet());
			if(address.getUse()!=null)
				if(address.getUse()==BuildingUse.WORKPLACE) {			//Anche qui sono presenti 
					var check1 = addresses.findByCustomerAndUse(address.getCustomer(), BuildingUse.WORKPLACE);
					if(check1.isEmpty())
						ad.setUse(BuildingUse.WORKPLACE);
				}else {
					var check = addresses.findByCustomerAndUse(address.getCustomer(), BuildingUse.REGISTERED_OFFICE);
					if(check.isEmpty())
						ad.setUse(BuildingUse.REGISTERED_OFFICE);
				}
			return addresses.save(ad);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}

	@Override//metodo di rimozione dal BD
	public Address delete(int id) {
		try {
			var ad = addresses.findById(id).get();
			addresses.delete(ad);
			return ad;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	//Seguono vari metodi per il recupero dal DB
	@Override
	public Address getById(int id) {
		try {
			return addresses.findById(id).get();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Address getByCityAndStreetAndHouseNumber(City city, String street, int houseNumber) {
		try {
			return addresses.findByCityAndStreetAndHouseNumber(city, street, houseNumber).get();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Address> getByCustomer(Customer customer, Pageable pageable) {
		try {
			return addresses.findByCustomer(customer, pageable);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Address> getByCity(City city, Pageable pageable) {
		try {
			return addresses.findByCity(city, pageable);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Address> getByPostalCode(String PostalCode, Pageable pageable) {
		try {
			return addresses.findByPostalCode(PostalCode, pageable);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}

	@Override
	public Page<Address> getAll(Pageable pageable) {
		try {
			return addresses.findAll(pageable);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}	}

	
	//Questo metodo viene richiamato nei servizi del cliente, nello specifico l'eliminazione di un cliente 
	//viene preceduta, qualora ci siano, dall'eliminazione di tutti gli indirizzi a lui associati
	@Override
	public int deleteByCustomer(Customer customer) {
		try {
			var x = addresses.findByCustomer(customer, null).getContent();
			int eliminati = 0;
			
			for(Address a : x) {
				addresses.delete(a);
				eliminati++;
			}
			
			return eliminati;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Address> getAll() {
		try {
			return addresses.findAll();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

}
