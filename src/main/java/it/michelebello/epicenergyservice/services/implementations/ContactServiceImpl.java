package it.michelebello.epicenergyservice.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.michelebello.epicenergyservice.models.data.Contact;
import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.repositories.ContactRepository;
import it.michelebello.epicenergyservice.services.ContactService;

@Service
public class ContactServiceImpl implements ContactService {

	@Autowired
	ContactRepository contacts;

	//Sono qui presenti le implementazioni dei servizi dei contatti
	
	@Override//metodo per l'aggiunta in DB
	public Contact save(Contact contact) {
		try {
			return contacts.save(contact);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo per la modifica (Sono presenti molti controlli per garantire che solo i campi inseriti dall'utente rientrino nelle modifiche)
	public Contact update(Contact contact) {
		try {
			var c = contacts.findById(contact.getId()).get();
			if (contact.getCustomer() != null)
				c.setCustomer(contact.getCustomer());
			if (contact.getEmail() != null)
				if (!contact.getEmail().isBlank())
					c.setEmail(contact.getEmail());
			if (contact.getName() != null)
				if (!contact.getName().isBlank())
					c.setName(contact.getName());
			if (contact.getSurname() != null)
				if (!contact.getSurname().isBlank())
					c.setSurname(contact.getSurname());
			if (contact.getPhoneNumber() != null)
				if (!contact.getPhoneNumber().isBlank())
					c.setPhoneNumber(contact.getPhoneNumber());

			return contacts.save(c);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override//metodo di rimozione dal BD
	public Contact delete(int id) {
		try {
			var c = contacts.findById(id).get();
			contacts.delete(c);
			return c;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	//Seguono vari metodi per il recupero dal DB

	@Override
	public Contact getByEmail(String email) {
		try {
			return contacts.findByEmail(email).get();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Contact getByPhoneNumber(String phoneNumber) {
		try {
			return contacts.findByPhoneNumber(phoneNumber).get();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Contact> getByName(String name, Pageable pageable) {
		try {
			return contacts.findByNameIgnoreCase(name, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Contact> getBySurmane(String surname, Pageable pageable) {
		try {
			return contacts.findBySurnameIgnoreCase(surname, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Contact> getByCustomer(Customer customer, Pageable pageable) {
		try {
			return contacts.findByCustomer(customer, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Contact> getAll(Pageable pageable) {
		try {
			return contacts.findAll(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int deleteByCustomer(Customer customer) {
		try {
			var cont = contacts.findByCustomer(customer, null).getContent();
			int i = 0;
			for(Contact c : cont) {
				contacts.delete(c);
				i++;
			}
			return i;
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

}
