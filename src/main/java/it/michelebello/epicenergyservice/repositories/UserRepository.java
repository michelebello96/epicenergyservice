package it.michelebello.epicenergyservice.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import it.michelebello.epicenergyservice.models.data.User;

//Interfaccia di comunicazione con il DB per gli utenti

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Integer>{

	List<User> findAll();
	Optional<User> findByEmail(String email);
	Optional<User> findByUsername(String username);
}
