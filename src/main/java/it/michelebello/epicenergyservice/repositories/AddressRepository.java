package it.michelebello.epicenergyservice.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import it.michelebello.epicenergyservice.citiesloader.models.City;
import it.michelebello.epicenergyservice.models.data.Address;
import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.models.enums.BuildingUse;

//Interfaccia di comunicazione con il DB per gli indirizzi

@Repository
public interface AddressRepository extends PagingAndSortingRepository<Address, Integer>{

	List<Address> findAll();
	List<Address> findByUse(BuildingUse use, Sort sort);
	Optional<Address> findByCityAndStreetAndHouseNumber(City city, String street, int houseNumber);
	Page<Address> findByCustomer(Customer c, Pageable page);
	Page<Address> findByCity(City city, Pageable page);
	Page<Address> findByPostalCode(String postalCode, Pageable page);
	Optional<Address> findByCustomerAndUse(Customer customer, BuildingUse use);
}
