package it.michelebello.epicenergyservice.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import it.michelebello.epicenergyservice.models.data.Role;
import it.michelebello.epicenergyservice.models.enums.RoleType;

//Interfaccia di comunicazione con il DB per i ruoli degli utenti

@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, Integer>{

	Optional<Role> findByRoleType(RoleType roletype);
}
