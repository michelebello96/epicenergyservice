package it.michelebello.epicenergyservice.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import it.michelebello.epicenergyservice.models.data.Customer;

//Interfaccia di comunicazione con il DB per i clienti

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer>{

	Page<Customer> findAll(Pageable page);
	List<Customer> findAll();
	Page<Customer> findByCreationDateAfterAndCreationDateBefore(LocalDate from, LocalDate to, Pageable page);
	Page<Customer> findByLastContactAfterAndLastContactBefore(LocalDate from, LocalDate to, Pageable page);
	Page<Customer> findByBusinessNameContainingIgnoreCase(String name, Pageable page);
	List<Customer> findAllByOrderByIdAsc();
	
}
