package it.michelebello.epicenergyservice.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import it.michelebello.epicenergyservice.models.data.Type;

//Interfaccia di comunicazione con il DB per il tipo delle società

@Repository
public interface TypeRepository extends PagingAndSortingRepository<Type, Integer>{

	Optional<Type> findByType(String type);
	Page<Type> findAll(Pageable pageable);
	List<Type> findAll();
	
}
