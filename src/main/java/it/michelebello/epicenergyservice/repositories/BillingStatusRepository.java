package it.michelebello.epicenergyservice.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import it.michelebello.epicenergyservice.models.data.BillingStatus;

//Interfaccia di comunicazione con il DB per lo stato delle fatture

@Repository
public interface BillingStatusRepository extends PagingAndSortingRepository<BillingStatus, Integer>{

	List<BillingStatus> findAll();
	Optional<BillingStatus> findByStatusIgnoreCase(String status);
}
