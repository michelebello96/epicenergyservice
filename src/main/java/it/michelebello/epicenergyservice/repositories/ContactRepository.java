package it.michelebello.epicenergyservice.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import it.michelebello.epicenergyservice.models.data.Contact;
import it.michelebello.epicenergyservice.models.data.Customer;

//Interfaccia di comunicazione con il DB per i contatti

@Repository
public interface ContactRepository extends PagingAndSortingRepository<Contact, Integer>{

	List<Contact> findAll();
	Optional<Contact> findByEmail(String email);
	Optional<Contact> findByPhoneNumber(String phoneNumber);
	Page<Contact> findByNameIgnoreCase(String name, Pageable pageable);
	Page<Contact> findBySurnameIgnoreCase(String surname,Pageable pageable);
	Page<Contact> findByCustomer(Customer customer, Pageable pageable);
}
