package it.michelebello.epicenergyservice.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import it.michelebello.epicenergyservice.models.data.Bill;
import it.michelebello.epicenergyservice.models.data.BillingStatus;
import it.michelebello.epicenergyservice.models.data.Customer;

//Interfaccia di comunicazione con il DB per le fatture

@Repository
public interface BillRepository extends PagingAndSortingRepository<Bill, Integer>{

	List<Bill> findAll();
	Page<Bill> findAll(Pageable pageable);
	Page<Bill> findByBillingDateAfterAndBillingDateBefore(LocalDate from, LocalDate to, Pageable page);
	Page<Bill> findByStatus(BillingStatus status, Pageable pageable);
	Page<Bill> findByCustomer(Customer customer, Pageable pageable);
	
}