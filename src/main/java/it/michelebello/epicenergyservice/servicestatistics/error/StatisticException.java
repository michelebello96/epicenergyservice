package it.michelebello.epicenergyservice.servicestatistics.error;

public class StatisticException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public StatisticException() {
		super("Error in statistical calculations ");
	}
}
