package it.michelebello.epicenergyservice.servicestatistics.implementation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.michelebello.epicenergyservice.citiesloader.models.Province;
import it.michelebello.epicenergyservice.models.data.Address;
import it.michelebello.epicenergyservice.models.data.Bill;
import it.michelebello.epicenergyservice.models.data.BillingStatus;
import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.services.AddressService;
import it.michelebello.epicenergyservice.services.BillService;
import it.michelebello.epicenergyservice.services.CustomerService;
import it.michelebello.epicenergyservice.servicestatistics.StatisticsService;
import it.michelebello.epicenergyservice.servicestatistics.error.StatisticException;

@Service
public class StatisticServiceImpl implements StatisticsService{

	@Autowired
	CustomerService customers;
	@Autowired
	AddressService addresses;
	@Autowired
	BillService bills;
	
	@Override
	public List<String> CustomersInProvinces() {
		try {
			List<String> sol = new LinkedList<String>();
			var ind = addresses.getAll();
			Map<Province,Double> m = new HashMap<Province, Double>();
			for(Address a : ind) {
				if(m.containsKey(a.getCity().getProvince())) {
					m.replace(a.getCity().getProvince(), m.get(a.getCity().getProvince())+1.0);
				}else {
					m.put(a.getCity().getProvince(),1.0);
				}
			}
			
			for(Province p : m.keySet()) {
				sol.add(p.getName() + " -> " + (double)Math.round(m.get(p)/ind.size()*10000)/100 + "%");
			}
			return sol;
		}catch(Exception e) {
			throw new StatisticException();
		}
	}

	@Override
	public List<String> StatusPerBills() {
		try {
			List<String> sol = new LinkedList<String>();
			var ind = bills.getAll();
			Map<BillingStatus,Double> m = new HashMap<BillingStatus, Double>();
			for(Bill b : ind) {
				if(m.containsKey(b.getStatus())) {
					m.replace(b.getStatus(), m.get(b.getStatus())+1.0);
				}else {
					m.put(b.getStatus(),1.0);
				}
			}
			
			for(BillingStatus b : m.keySet()) {
				sol.add(b.getStatus() + " -> " + (double)Math.round(m.get(b)/ind.size()*10000)/100 + "%");
			}
			
			return sol;
		}catch(Exception e) {
			throw new StatisticException();
		}
	}

	@Override
	public List<String> BuildingsPerCustomer() {
		try {
			List<String> sol = new LinkedList<String>();
			var ind = addresses.getAll();
			Map<Customer, Integer> m = new HashMap<Customer, Integer>();
			for(Address b : ind) {
				if(m.containsKey(b.getCustomer())) {
					m.replace(b.getCustomer(), m.get(b.getCustomer())+1);
				}else {
					m.put(b.getCustomer(),1);
				}
			}
			
			var cust = customers.getAll();
			double tot = cust.size();
			double uno = 0, due = 0;
			for(Customer c : m.keySet()) {
				if(m.get(c) == 1 ) {
					cust.remove(c);
					uno++; 
				}
				else{
					cust.remove(c);
					due++; 	
				}
			}
			
			sol.add("Zero sedi -> " + (double)Math.round(cust.size()/tot*10000)/100 + "%");
			sol.add("Una sede -> " + (double)Math.round(uno/tot*10000)/100 + "%");
			sol.add("Due sedi -> " + (double)Math.round(due/tot*10000)/100 + "%");
			return sol;
		}catch(Exception e) {
			throw new StatisticException();
		}
	}

	
	
}