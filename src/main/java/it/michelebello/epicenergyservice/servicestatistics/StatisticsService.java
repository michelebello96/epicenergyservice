package it.michelebello.epicenergyservice.servicestatistics;

import java.util.List;

public interface StatisticsService {

	List<String> CustomersInProvinces();
	List<String> StatusPerBills();
	List<String> BuildingsPerCustomer();
	
}
