package it.michelebello.epicenergyservice.controllers;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.michelebello.epicenergyservice.citiesloader.services.CitiesService;
import it.michelebello.epicenergyservice.controllers.models.AddressRequest;
import it.michelebello.epicenergyservice.controllers.models.BillRequest;
import it.michelebello.epicenergyservice.models.data.Address;
import it.michelebello.epicenergyservice.models.enums.BuildingUse;
import it.michelebello.epicenergyservice.services.AddressService;
import it.michelebello.epicenergyservice.services.CustomerService;

@Controller
@RequestMapping("/addrmvc")
public class AddressMVC {

	@Autowired
	AddressService addresses;
	@Autowired
	CustomerService customers;
	@Autowired
	CitiesService cities;
	
	@GetMapping("/aggiungi")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView newAddress(Model m) {
		m.addAttribute("address", new AddressRequest());
		m.addAttribute("newAddress", 1);
		return new ModelAndView("newAddress", m.asMap());
	}
	
	@GetMapping("/modifica")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView updateAddress(Model m) {
		m.addAttribute("address", new AddressRequest());
		m.addAttribute("update", 1);
		return new ModelAndView("newAddress", m.asMap());
	}
	
	@GetMapping("/rimuovi")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView deleteAddress(Model m) {
		m.addAttribute("delete", 1);
		return new ModelAndView("newAddress", m.asMap());
	}
	
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String save(AddressRequest address) {
		Address a = Address.builder()
				.withHouseNumber(address.getNumber())
				.withLocality(address.getLocality())
				.withPostalCode(address.getPostalCode())
				.withStreet(address.getStreet())
				.withUse(address.isRegistredOffice() ? BuildingUse.REGISTERED_OFFICE : BuildingUse.WORKPLACE)
				.build();
		a.setCustomer(customers.getById(address.getCustomerId()));
		if(address.getCity()!=null && address.getProvinceAcronym()!=null)
			a.setCity(cities.getByName(address.getCity(), address.getProvinceAcronym()).get());
		
		addresses.save(a);
		return "home.html";
	}
	
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String update(AddressRequest address) {
		Address a = Address.builder()
				.withHouseNumber(address.getNumber())
				.withLocality(address.getLocality())
				.withPostalCode(address.getPostalCode())
				.withStreet(address.getStreet())
				.withUse(address.isRegistredOffice() ? BuildingUse.REGISTERED_OFFICE : BuildingUse.WORKPLACE)
				.build();
		a.setId(address.getId());
		if(address.getCustomerId()!=null)
			a.setCustomer(customers.getById(address.getCustomerId()));
		if(!address.getCity().isBlank() && !address.getProvinceAcronym().isBlank())
			a.setCity(cities.getByName(address.getCity(), address.getProvinceAcronym()).get());
		
		addresses.update(a);
		return "home.html";
	}
	
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String delete(Integer id) {
		addresses.delete(id);
		return "home.html";
	}
}
