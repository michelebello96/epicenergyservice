package it.michelebello.epicenergyservice.controllers;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.michelebello.epicenergyservice.controllers.models.RegistrationRequest;
import it.michelebello.epicenergyservice.models.data.Role;
import it.michelebello.epicenergyservice.models.data.User;
import it.michelebello.epicenergyservice.models.enums.RoleType;
import it.michelebello.epicenergyservice.repositories.RoleRepository;
import it.michelebello.epicenergyservice.services.RoleService;
import it.michelebello.epicenergyservice.services.UserService;

@Controller
@RequestMapping("/usermvc")
public class UserMVC {

	@Autowired
	UserService users;
	@Autowired
	RoleService roles;
	
	@GetMapping("/modifica")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView updateUser(Model m) {
		m.addAttribute("user", new RegistrationRequest());
		m.addAttribute("update", 1);
		return new ModelAndView("newUser", m.asMap());
	}
	
	@GetMapping("/rimuovi")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView deleteUser(Model m) {
		m.addAttribute("delete", 1);
		return new ModelAndView("newUser", m.asMap());
	}
	
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String update(RegistrationRequest reg){
		
		User u = User.builder()
				.withEmail(reg.getEmail())
				.withName(reg.getName())
				.withPassword(reg.getPassword())
				.withSurname(reg.getSurname())
				.withUsername(reg.getUsername())
				.withRoles(new HashSet<Role>())
				.build();
		u.getRoles().add(roles.getByRoleType(RoleType.ROLE_USER));
		if(reg.isAdmin())
			u.getRoles().add(roles.getByRoleType(RoleType.ROLE_ADMIN));		
		u.setId(reg.getId());
		
		users.update(u);
		
		return "home.html";
	}
	
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String delete(int id) {
		users.delete(id);
		return "home.html";
	}
	
}
