package it.michelebello.epicenergyservice.controllers;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import it.michelebello.epicenergyservice.controllers.models.CustomerRequest;
import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.repositories.TypeRepository;
import it.michelebello.epicenergyservice.services.CustomerService;

@Controller
@RequestMapping("/custmvc")
public class CustomerMVC {

	@Autowired
	CustomerService customers;
	@Autowired
	TypeRepository types;
	
	@GetMapping("/aggiungi")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView newCustomer(Model m) {
		m.addAttribute("customer", new CustomerRequest());
		m.addAttribute("newCust", 1);
		m.addAttribute("types", types.findAll());
		return new ModelAndView("newCustomer", m.asMap());
	}
	
	@GetMapping("/modifica")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView updateCustomer(Model m) {
		m.addAttribute("customer", new CustomerRequest());
		m.addAttribute("update", 1);
		m.addAttribute("types", types.findAll());
		return new ModelAndView("newCustomer", m.asMap());
	}
	
	@GetMapping("/rimuovi")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView deleteCustomer(Model m) {
		m.addAttribute("delete", 1);
		return new ModelAndView("newCustomer", m.asMap());
	}
	
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String save(CustomerRequest customer) {
		Customer c = Customer.builder()
				.withBusinessEmail(customer.getBusinessEmail())
				.withBusinessName(customer.getBusinessName())
				.withPec(customer.getPec())
				.withPhoneNumber(customer.getPhoneNumber())
				.withVatNumber(customer.getVatNumber())
				.build();
		if(customer.getAnnualRevenue()!=null)
			c.setAnnualRevenue(BigDecimal.valueOf(customer.getAnnualRevenue()));
		if(customer.getType()!=null)
			c.setType(types.findByType(customer.getType()).get());
		
		customers.save(c);
		
		return "home.html";
	}
	
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String update(CustomerRequest customer) {
		Customer c = Customer.builder()
				.withBusinessEmail(customer.getBusinessEmail())
				.withBusinessName(customer.getBusinessName())
				.withPec(customer.getPec())
				.withPhoneNumber(customer.getPhoneNumber())
				.withVatNumber(customer.getVatNumber())
				.build();
		if(customer.getAnnualRevenue()!=null)
			c.setAnnualRevenue(BigDecimal.valueOf(customer.getAnnualRevenue()));
		if(customer.getType()!=null)
			c.setType(types.findByType(customer.getType()).get());
		
		c.setId(customer.getId());
		
		customers.update(c);
		return "home.html";
	}
	
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String delete(int id) {
		customers.delete(id);
		return "home.html";
	}
}
