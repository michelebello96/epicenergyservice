package it.michelebello.epicenergyservice.controllers.models;

import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * Questa classe rappresenta una versione semplificata dell'indirizzo 
 * che viene utilizzata per gli scambi con il client che si interfaccia 
 * ai controllers
 */

@Data
@NoArgsConstructor
public class AddressRequest {

	private Integer id;
	private String street;
	private Integer number;
	private String locality;
	private String postalCode;
	private String city;
	private String provinceAcronym;
	private Integer customerId;
	private boolean registredOffice;
}
