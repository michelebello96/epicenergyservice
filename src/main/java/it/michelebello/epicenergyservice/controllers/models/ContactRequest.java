package it.michelebello.epicenergyservice.controllers.models;

import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * Questa classe rappresenta una versione semplificata del contatto 
 * che viene utilizzata per gli scambi con il client che si interfaccia 
 * ai controllers
 */

@Data
@NoArgsConstructor
public class ContactRequest {

	private Integer id;
	private String name;
	private String surname;
	private String email;
	private String phoneNumber;
	private Integer idCustomer;
	
}
