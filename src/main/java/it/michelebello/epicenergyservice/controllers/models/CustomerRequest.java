package it.michelebello.epicenergyservice.controllers.models;

import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * Questa classe rappresenta una versione semplificata del cliente 
 * che viene utilizzata per gli scambi con il client che si interfaccia 
 * ai controllers
 */

@Data
@NoArgsConstructor
public class CustomerRequest {

	private Integer id;
	private String businessName;
	private String vatNumber;
	private String businessEmail;
	private Double annualRevenue;
	private String pec;
	private String phoneNumber;
	private String type;
	
}
