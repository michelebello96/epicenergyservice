package it.michelebello.epicenergyservice.controllers.models;

import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * Questa classe rappresenta una versione semplificata dell'utente 
 * che viene utilizzata per gli scambi con il client che si interfaccia 
 * ai controllers
 */

@Data
@NoArgsConstructor
public class RegistrationRequest {

	private Integer id;
	private String username;
	private String email;
	private String password;
	private String name;
	private String surname;
	private boolean admin;
	
}
