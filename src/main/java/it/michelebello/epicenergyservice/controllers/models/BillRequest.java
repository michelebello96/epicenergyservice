package it.michelebello.epicenergyservice.controllers.models;

import lombok.Data;

/*
 * Questa classe rappresenta una versione semplificata della fattura
 * che viene utilizzata per gli scambi con il client che si interfaccia 
 * ai controllers
 */

@Data
public class BillRequest {

	private Integer id;
	private Integer year;
	private Integer month;
	private Integer day;
	private Double amount;
	private Integer number;
	private String status;
	private Integer customerId;
	
}
