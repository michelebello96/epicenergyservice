package it.michelebello.epicenergyservice.controllers.models;

/*
 * Questa classe funge da payload per i dati necessari
 * all'autenticazione dell'utente per l'utilizzo dei servizi
 */

public class LoginRequest {	
	private String username;	
	private String password;
	
	
	public LoginRequest(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}