package it.michelebello.epicenergyservice.controllers.models;

import java.util.Date;
import java.util.List;
/*
 * Questa classe fornisce i dati successivi all'autenticazione
 * e in particola un token che permetterà poi l'uso degli end-point 
 * con impostata una preautorizzazione
 */
public class LoginResponse {
	// Token
	private String token;	
	// Imposta il prefisso che indica il tipo di Token
	private final String type = "Bearer";
	// Dati dell'utente
	private int id;
	private String username;
	private String email;
	private List<String> roles;
	private Date expirationTime;
	
	
	public LoginResponse(String token, int id, String username, String email, List<String> roles,
			Date expirationTime) {
		super();
		this.token = token;
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
		this.expirationTime = expirationTime;
	}


	public String getToken() {
		return token;
	}


	public String getType() {
		return type;
	}


	public int getId() {
		return id;
	}


	public String getUsername() {
		return username;
	}


	public String getEmail() {
		return email;
	}


	public List<String> getRoles() {
		return roles;
	}


	public Date getExpirationTime() {
		return expirationTime;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public void setId(int id) {
		this.id = id;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public void setRoles(List<String> roles) {
		this.roles = roles;
	}


	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}
	
	
}
