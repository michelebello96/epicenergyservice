package it.michelebello.epicenergyservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.michelebello.epicenergyservice.controllers.models.BillRequest;
import it.michelebello.epicenergyservice.controllers.models.ContactRequest;
import it.michelebello.epicenergyservice.models.data.Contact;
import it.michelebello.epicenergyservice.services.ContactService;
import it.michelebello.epicenergyservice.services.CustomerService;

@Controller
@RequestMapping("/contmvc")
public class ContactMVC {
	
	@Autowired
	ContactService contacts;
	@Autowired
	CustomerService customers;
	
	@GetMapping("/aggiungi")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView newContact(Model m) {
		m.addAttribute("contact", new ContactRequest());
		m.addAttribute("newCont", 1);
		return new ModelAndView("newContact", m.asMap());
	}
	
	@GetMapping("/modifica")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView updateContact(Model m) {
		m.addAttribute("contact", new ContactRequest());
		m.addAttribute("update", 1);
		return new ModelAndView("newContact", m.asMap());
	}
	
	@GetMapping("/rimuovi")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView deleteContact(Model m) {
		m.addAttribute("delete", 1);
		return new ModelAndView("newContact", m.asMap());
	}	

	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String save(ContactRequest contact) {
		Contact c = Contact.builder()
				.withEmail(contact.getEmail())
				.withName(contact.getName())
				.withSurname(contact.getSurname())
				.withPhoneNumber(contact.getPhoneNumber())
				.withCustomer(customers.getById(contact.getIdCustomer()))
				.build();
					
		contacts.save(c);
		return "home.html";
	}
	
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String update(ContactRequest contact) {
		Contact c = Contact.builder()
				.withEmail(contact.getEmail())
				.withName(contact.getName())
				.withSurname(contact.getSurname())
				.withPhoneNumber(contact.getPhoneNumber())
				.build();
		c.setId(contact.getId());
		if(contact.getIdCustomer()!=null)
			c.setCustomer(customers.getById(contact.getIdCustomer()));
		
		contacts.update(c);
		return "home.html";
	}
	
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String delete(@RequestParam int id) {
		contacts.delete(id);
		return "home.html";
	}
}
