package it.michelebello.epicenergyservice.controllers;

import java.util.Collections;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.michelebello.epicenergyservice.citiesloader.services.CitiesService;
import it.michelebello.epicenergyservice.controllers.models.BillRequest;
import it.michelebello.epicenergyservice.controllers.models.CustomerRequest;
import it.michelebello.epicenergyservice.controllers.models.LoginRequest;
import it.michelebello.epicenergyservice.controllers.models.RegistrationRequest;
import it.michelebello.epicenergyservice.models.data.User;
import it.michelebello.epicenergyservice.models.enums.RoleType;
import it.michelebello.epicenergyservice.repositories.AddressRepository;
import it.michelebello.epicenergyservice.repositories.BillRepository;
import it.michelebello.epicenergyservice.repositories.ContactRepository;
import it.michelebello.epicenergyservice.repositories.CustomerRepository;
import it.michelebello.epicenergyservice.repositories.UserRepository;
import it.michelebello.epicenergyservice.security.JwtUtils;
import it.michelebello.epicenergyservice.security.service.UserDetailsImpl;
import it.michelebello.epicenergyservice.services.BillService;
import it.michelebello.epicenergyservice.services.CustomerService;
import it.michelebello.epicenergyservice.services.CustomerSorterService;
import it.michelebello.epicenergyservice.services.RoleService;
import it.michelebello.epicenergyservice.services.UserService;
import it.michelebello.epicenergyservice.servicestatistics.StatisticsService;


@Controller
@RequestMapping("/mvc")
public class ControllerMVC {

	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	JwtUtils jwtUtils;
	@Autowired
	RoleService roles;
	@Autowired
	UserRepository users;
	@Autowired
	CustomerRepository customers;
	@Autowired
	BillRepository bills;
	@Autowired
	CitiesService cities;
	@Autowired
	ContactRepository contacts;
	@Autowired
	AddressRepository addresses;
	@Autowired
	StatisticsService statistics;
	
	@PostMapping("/login")
	public ModelAndView authenticateUser(@RequestParam String username, @RequestParam String password,
			HttpServletResponse response, Model model) {
		LoginRequest loginRequest = new LoginRequest(username, password);

		Authentication authentication = authenticationManager.authenticate(
			new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		authentication.getAuthorities();

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		Cookie auth = new Cookie("token", jwt);
		auth.setSecure(true);
		auth.setHttpOnly(true);
		auth.setPath("/");
		response.addCookie(auth);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		if(userDetails.getAuthorities().size()>1)
			model.addAttribute("admin", 1);

		model.addAttribute("welcome", "Benvenuto "+username+" in Epic Energy Service!");
		
		return new ModelAndView("home", model.asMap());
	}
	
	@PostMapping("/register")
	public String register(RegistrationRequest model) {
		User user = User.builder()
				.withEmail(model.getEmail())
				.withName(model.getName())
				.withPassword(model.getPassword())
				.withSurname(model.getSurname())
				.withUsername(model.getUsername())
				.build();
		

		if(model.isAdmin())
			user.getRoles().add(roles.getByRoleType(RoleType.ROLE_ADMIN));
		
		users.save(user);
		return "redirect:/index.html";
	}
	
	@GetMapping("/registerlink")
	public ModelAndView registazionelink(Model m) {
		m.addAttribute("user", new RegistrationRequest());
		return new ModelAndView("registrazione", m.asMap());
	}
	
	@GetMapping("/customers")
	@PreAuthorize("hasRole('USER')")
	public ModelAndView customerslink(Model m) {
		var c = customers.findAllByOrderByIdAsc();
		m.addAttribute("customers", c);
		return new ModelAndView("customers", m.asMap());
	}
	
	@GetMapping("/bills")
	@PreAuthorize("hasRole('USER')")
	public ModelAndView billslink(Model m) {
		var b = bills.findAll();
		m.addAttribute("bills", b);
		return new ModelAndView("bills", m.asMap());
	}
	
	@GetMapping("/cities")
	@PreAuthorize("hasRole('USER')")
	public ModelAndView citylink(Model m) {
		var c = cities.getAll();
		Collections.sort(c);
		m.addAttribute("cities", c);
		return new ModelAndView("cities", m.asMap());
	}
	
	@GetMapping("/users")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView userslink(Model m) {
		var u = users.findAll();
		m.addAttribute("users", u);
		return new ModelAndView("users", m.asMap());
	}
	
	@GetMapping("/contacts")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView contactslink(Model m) {
		var c = contacts.findAll();
		m.addAttribute("contacts", c);
		return new ModelAndView("contacts", m.asMap());
	}
	
	@GetMapping("/addresses")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView addresseslink(Model m) {
		var u = addresses.findAll();
		m.addAttribute("addresses", u);
		return new ModelAndView("addresses", m.asMap());
	}
	
	@GetMapping("/statistics")
	@PreAuthorize("hasRole('USER')")
	public ModelAndView statisticslink(Model m) {
		m.addAttribute("customersInProvince", statistics.CustomersInProvinces());
		m.addAttribute("buildingsPerCostumer", statistics.BuildingsPerCustomer());
		m.addAttribute("statusPerBill", statistics.StatusPerBills());
		return new ModelAndView("statistics", m.asMap());
	}
	
	@GetMapping("/home")
	@PreAuthorize("hasRole('USER')")
	public String goHome() {
		return "home.html";
	}
}
