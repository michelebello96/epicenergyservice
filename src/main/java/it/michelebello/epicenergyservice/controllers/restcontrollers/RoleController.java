package it.michelebello.epicenergyservice.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.michelebello.epicenergyservice.models.data.Role;
import it.michelebello.epicenergyservice.services.RoleService;

/*
 * Questo controller permette di interfacciarsi con i 
 * ruoli degli utenti del servizio.
 * L'unico suo metodo permete di visualizzare i ruoli
 * che sono stati inseriti nel servizio in precedenza tramite runner
 */
@RestController
@RequestMapping("/roles")
public class RoleController {

	@Autowired
	RoleService roles;
	
	@GetMapping("/all")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Page<Role> getAll(@RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return roles.getAll(PageRequest.of(pag, size, Sort.by(sort)));
	}
	
}
