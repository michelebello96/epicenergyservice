package it.michelebello.epicenergyservice.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.michelebello.epicenergyservice.citiesloader.models.City;
import it.michelebello.epicenergyservice.citiesloader.services.CitiesService;
import it.michelebello.epicenergyservice.controllers.models.AddressRequest;
import it.michelebello.epicenergyservice.models.data.Address;
import it.michelebello.epicenergyservice.models.enums.BuildingUse;
import it.michelebello.epicenergyservice.services.AddressService;
import it.michelebello.epicenergyservice.services.CustomerService;

/*
 * In questa classe sono racchiusi gli end-point che riguardano
 * gli indirizzi dei clienti del servizio
 */

@RestController
@RequestMapping("/addresses")
public class AddressController {
	
	@Autowired
	AddressService addresses;
	@Autowired
	CitiesService cities;
	@Autowired
	CustomerService customers;
	
	/*
	 * Il metodo per il salvataggio di un nuovo indirizzo,
	 * tramite il POJO AddressRequest, va a comporre
	 * l'indirizzo vero e proprio con tutti i collegamenti
	 * e le complessità annesse 
	 */
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Address save(@RequestBody AddressRequest address) {
		Address a = Address.builder()
				.withHouseNumber(address.getNumber())
				.withLocality(address.getLocality())
				.withPostalCode(address.getPostalCode())
				.withStreet(address.getStreet())
				.withUse(address.isRegistredOffice() ? BuildingUse.REGISTERED_OFFICE : BuildingUse.WORKPLACE)
				.build();
		//Si richiama il cliente proprietario dell'indirizzo senza controllo così che in caso di assenza non venga salvato
		//un'indirizzo senza proprietario nel DB
		a.setCustomer(customers.getById(address.getCustomerId()));
		if(address.getCity()!=null && address.getProvinceAcronym()!=null)
			a.setCity(cities.getByName(address.getCity(), address.getProvinceAcronym()).get());
		
		return addresses.save(a);
	}
	
	/*
	 * Discorso analogo al salvataggio vale per la modifica
	 * in cui il POJO viene "analizzato" e si sviluppa il modello
	 * adatto per inserire poi la modifica in DB
	 */
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Address update(@RequestBody AddressRequest address) {
		Address a = Address.builder()
				.withHouseNumber(address.getNumber())
				.withLocality(address.getLocality())
				.withPostalCode(address.getPostalCode())
				.withStreet(address.getStreet())
				.withUse(address.isRegistredOffice() ? BuildingUse.REGISTERED_OFFICE : BuildingUse.WORKPLACE)
				.build();
		a.setId(address.getId());
		if(address.getCustomerId()!=null)
			a.setCustomer(customers.getById(address.getCustomerId()));
		if(address.getCity()!=null && address.getProvinceAcronym()!=null)
			a.setCity(cities.getByName(address.getCity(), address.getProvinceAcronym()).get());
		
		return addresses.update(a);
	}
	
	/*
	 * Per l'eliminazione si ricorre invece ad un parametro che 
	 * rappresenta l'id dell'elemento da eliminare.
	 * Tale elemento è facilmente reperibile tramite gli end-point 
	 * di recupero dati
	 */
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Address delete(@RequestParam Integer id) {
		return addresses.delete(id);
	}

	
	/*
	 * Seguono vari metodi per il recupero degli indirizzi
	 * con vari vincoli
	 */
	@GetMapping("/bycity/{acronym}/{city}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Address> getByCity(@PathVariable String acronym, @PathVariable String city, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size) {
		City c = cities.getByName(city, acronym).get();
		return addresses.getByCity(c, PageRequest.of(pag, size));
	}
	
	@GetMapping("/bycustomer")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Address> getByCustomer(@RequestParam int idCustomer, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size){
		var c = customers.getById(idCustomer);
		return addresses.getByCustomer(c, PageRequest.of(pag, size));
	}
	
	@GetMapping("/bypostalcode")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Address> getByPostalCode(@RequestParam String postalCode, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size){
		return addresses.getByPostalCode(postalCode, PageRequest.of(pag, size));
	}
	
	@GetMapping("/all")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Address> getAll(@RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return addresses.getAll(PageRequest.of(pag, size, Sort.by(sort)));
	}
	
}
