package it.michelebello.epicenergyservice.controllers.restcontrollers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.michelebello.epicenergyservice.controllers.models.CustomerRequest;
import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.services.CustomerService;
import it.michelebello.epicenergyservice.services.CustomerSorterService;
import it.michelebello.epicenergyservice.services.TypeService;

/*
 * In questa classe sono racchiusi gli end-point che riguardano
 * i clienti del servizio
 */
@RestController
@RequestMapping("/customers")
public class CustomerController {
	
	@Autowired
	CustomerService customers;
	@Autowired
	CustomerSorterService sortedCustomers;
	@Autowired
	TypeService types;
	
	/*
	 * Il metodo per il salvataggio di un nuovo cliente,
	 * tramite il POJO CustomerRequest, va a comporre
	 * il cliente vero e proprio con tutti i collegamenti
	 * e le complessità annesse 
	 */
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Customer save(@RequestBody CustomerRequest customer) {
		Customer c = Customer.builder()
				.withBusinessEmail(customer.getBusinessEmail())
				.withBusinessName(customer.getBusinessName())
				.withPec(customer.getPec())
				.withPhoneNumber(customer.getPhoneNumber())
				.withVatNumber(customer.getVatNumber())
				.build();
		if(customer.getAnnualRevenue()!=null)
			c.setAnnualRevenue(BigDecimal.valueOf(customer.getAnnualRevenue()));
		if(customer.getType()!=null)
			c.setType(types.getByType(customer.getType()));
		
		return customers.save(c);
	}
	
	/*
	 * Discorso analogo al salvataggio vale per la modifica
	 * in cui il POJO viene "analizzato" e si sviluppa il modello
	 * adatto per inserire poi la modifica in DB
	 */
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Customer update(@RequestBody CustomerRequest customer) {
		Customer c = Customer.builder()
				.withBusinessEmail(customer.getBusinessEmail())
				.withBusinessName(customer.getBusinessName())
				.withPec(customer.getPec())
				.withPhoneNumber(customer.getPhoneNumber())
				.withVatNumber(customer.getVatNumber())
				.build();
		if(customer.getAnnualRevenue()!=null)
			c.setAnnualRevenue(BigDecimal.valueOf(customer.getAnnualRevenue()));
		if(customer.getType()!=null)
			c.setType(types.getByType(customer.getType()));
		
		c.setId(customer.getId());
		
		return customers.update(c);
	}
	
	/*
	 * Per l'eliminazione si ricorre invece ad un parametro che 
	 * rappresenta l'id dell'elemento da eliminare.
	 * Tale elemento è facilmente reperibile tramite gli end-point 
	 * di recupero dati
	 */
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Customer delete(@RequestParam int id) {
		return customers.delete(id);
	}
	
	/*
	 * Seguono vari metodi per il recupero degli indirizzi
	 * con vari vincoli
	 */
	@GetMapping("/byId")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Customer getById(@RequestParam int id) {
		return customers.getById(id);
	}
	
	@GetMapping("/byrevenue")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Customer> getByAnnualRevenueRange(@RequestParam(defaultValue = "0.0") double from, @RequestParam double to,@RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return customers.getByAnnualRevenueRange(BigDecimal.valueOf(from), BigDecimal.valueOf(to), PageRequest.of(pag, size, Sort.by(sort)));	
	}
	
	@GetMapping("/bycreation")
	@PreAuthorize("hasRole('ROLE_USER')")//anche qui si è usato un formattatore per le date che entrano come stringa
	public Page<Customer> getByCreationRange(@RequestParam String from, @RequestParam String to, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		return customers.getByCreationDateRange(LocalDate.parse(from, formatter), LocalDate.parse(to, formatter), PageRequest.of(pag, size, Sort.by(sort)));	
	}
	
	@GetMapping("/bylastcontact")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Customer> getByLastContactRange(@RequestParam String from, @RequestParam String to, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		return customers.getByLastContactRange(LocalDate.parse(from, formatter), LocalDate.parse(to, formatter), PageRequest.of(pag, size, Sort.by(sort)));	
	}
	
	@GetMapping("/byname")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Customer> getByAnnualRevenueRange(@RequestParam String name, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return customers.getByNameContain(name, PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	
	/* sortIndex indica l'indice di ordinamento secondo l'ordine
	*	1 -> Nome
	*	2 -> Fattuarto annuo
	*	3 -> Data di creazione
	*	4 -> Data ultimo contatto
	*	5 -> Provincia della sede legale
	**/
	@GetMapping("/all")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Customer> getAllSorted(@RequestParam(defaultValue = "1") int sortIndex, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size){
		return sortedCustomers.getAllSorted(PageRequest.of(pag, size), sortIndex);
	}

}
