package it.michelebello.epicenergyservice.controllers.restcontrollers;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.michelebello.epicenergyservice.controllers.models.RegistrationRequest;
import it.michelebello.epicenergyservice.models.data.Role;
import it.michelebello.epicenergyservice.models.data.User;
import it.michelebello.epicenergyservice.models.enums.RoleType;
import it.michelebello.epicenergyservice.services.RoleService;
import it.michelebello.epicenergyservice.services.UserService;
/*
 * End-points destinati solo agli amministratori del servizio
 * che permette di gestire gli utenti
 */
@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService users;
	@Autowired
	RoleService roles;
	
	/*
	 * Permette qui di modificare un utente sempre con l'utilizzo
	 * di un POJO più seplice per poi andare a strutturare
	 * l'utente complesso
	 */
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public User update(@RequestBody RegistrationRequest reg){
		
		User u = User.builder()
				.withEmail(reg.getEmail())
				.withName(reg.getName())
				.withPassword(reg.getPassword())
				.withSurname(reg.getSurname())
				.withUsername(reg.getUsername())
				.withRoles(new HashSet<Role>())
				.build();
		u.getRoles().add(roles.getByRoleType(RoleType.ROLE_USER));
		if(reg.isAdmin())
			u.getRoles().add(roles.getByRoleType(RoleType.ROLE_ADMIN));		
		u.setId(reg.getId());
		
		return users.update(u);
	}
	
	/*
	 * Anche qui il delete è gestito tramite parametro quale l'id
	 */
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public User delete(@RequestParam int id) {
		return users.delete(id);
	}
	
	/*
	 * Seguono i metodi di recuper dei dati.
	 * Il primo restituisce l'elenco di tutti gli utenti.
	 * Il secondo permette il recupero singolo tramite id.
	 */
	@GetMapping("/all")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Page<User> getAll(@RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return users.getAll(PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	@GetMapping("/byid")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public User getById(@RequestParam int id) {
		return users.getById(id);
	}
}
