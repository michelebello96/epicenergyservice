package it.michelebello.epicenergyservice.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.michelebello.epicenergyservice.models.data.BillingStatus;
import it.michelebello.epicenergyservice.services.BillingStatusService;

/*
 * In questa classe sono racchiusi gli end-point che riguardano
 * gli stati di fatturazione delle fatture del servizio
 */

@RestController
@RequestMapping("/statuses")
public class BillingStatusController {

	@Autowired
	BillingStatusService statuses;
	
	/*
	 * E' l'unico metodo di recupero presente e fornisce la lista
	 * completa degli stati 
	 */
	@GetMapping("/all")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<BillingStatus> getAll(@RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return statuses.getAll(PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	
	/*
	 * Il metodo per il salvataggio di un nuovo stato.
	 * Data la semplicità della classe di riferimento non si 
	 * è usato qui alcuna classe di appoggio per la comuicazione
	 * con il client che si interfaccia 
	 */
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public BillingStatus save(@RequestBody BillingStatus status) {
		BillingStatus newStatus = new BillingStatus();
		newStatus.setStatus(status.getStatus());
		return statuses.save(newStatus);
	}
	
	/*
	 * Discorso analogo al salvataggio vale per la modifica
	 */
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public BillingStatus update(@RequestBody BillingStatus status) {
		return statuses.update(status);
	}
	
	/*
	 * Per l'eliminazione si ricorre invece ad un parametro che 
	 * rappresenta l'id dell'elemento da eliminare.
	 */
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public BillingStatus delete(@RequestParam int id) {
		return statuses.delete(id);
	}
}
