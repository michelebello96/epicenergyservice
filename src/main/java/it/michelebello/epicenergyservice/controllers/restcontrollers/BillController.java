package it.michelebello.epicenergyservice.controllers.restcontrollers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.michelebello.epicenergyservice.controllers.models.BillRequest;
import it.michelebello.epicenergyservice.models.data.Bill;
import it.michelebello.epicenergyservice.services.BillService;
import it.michelebello.epicenergyservice.services.BillingStatusService;
import it.michelebello.epicenergyservice.services.CustomerService;
/*
 * In questa classe sono racchiusi gli end-point che riguardano
 * le fatture dei clienti del servizio
 */

@RestController
@RequestMapping("/bills")
public class BillController {
	
	@Autowired
	BillService bills;
	@Autowired
	BillingStatusService statuses;
	@Autowired
	CustomerService customers;
	
	
	/*
	 * Il metodo per il salvataggio di una nuova fattura,
	 * tramite il POJO BillRequest va a comporre
	 * la fattura vera e propria con tutti i collegamenti
	 * e le complessità annesse 
	 */
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Bill save(@RequestBody BillRequest bill) {
		var b = Bill.builder()
				.withBillingNumber(bill.getNumber())
				.withCustomer(customers.getById(bill.getCustomerId()))
				.build();
		if(bill.getYear()!=null && bill.getMonth()!=null && bill.getDay()!=null)
			b.setBillingDate(LocalDate.of(bill.getYear(), bill.getMonth(), bill.getDay()));
		if(bill.getAmount()!=null)
			b.setAmount(BigDecimal.valueOf(bill.getAmount()));
		if(bill.getStatus()!=null)
			b.setStatus(statuses.getByStatus(bill.getStatus()));
		
		return bills.save(b);
	}

	/*
	 * Discorso analogo al salvataggio vale per la modifica
	 * in cui il POJO viene "analizzato" e si sviluppa il modello
	 * adatto per inserire poi la modifica in DB
	 */
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Bill update(@RequestBody BillRequest bill) {
		var b = Bill.builder()
				.withBillingNumber(bill.getNumber())
				.build();
		if(bill.getYear()!=null && bill.getMonth()!=null && bill.getNumber()!=null)
			b.setBillingDate(LocalDate.of(bill.getYear(), bill.getMonth(), bill.getDay()));
		if(bill.getAmount()!=null)
			b.setAmount(BigDecimal.valueOf(bill.getAmount()));
		b.setId(bill.getId());
		if(bill.getCustomerId()!=null)
			b.setCustomer(customers.getById(bill.getCustomerId()));
		if(bill.getStatus()!=null)
			b.setStatus(statuses.getByStatus(bill.getStatus()));
		
		return bills.update(b);		
	}
	
	/*
	 * Per l'eliminazione si ricorre invece ad un parametro che 
	 * rappresenta l'id dell'elemento da eliminare.
	 * Tale elemento è facilmente reperibile tramite gli end-point 
	 * di recupero dati
	 */
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Bill delete(@RequestParam Integer id) {
		return bills.delete(id);
	}
	
	/*
	 * Seguono vari metodi per il recupero delle fatture
	 * con vari vincoli
	 */
	@GetMapping("/bydates")
	@PreAuthorize("hasRole('ROLE_USER')")//In questo metodo è usato un formattatore di date per il recupero della data inserita come stringa nel formato "anno4cifre/mese2cifre/giorno2cifre"
	public Page<Bill> getBetweenDates(@RequestParam String from,@RequestParam String to,
    @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		return bills.getBetweenDates(LocalDate.parse(from,formatter), LocalDate.parse(to, formatter), PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	@GetMapping("/byyear")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Bill> getByYear(@RequestParam int year, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return bills.getByYear(year, PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	@GetMapping("/byamount")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Bill> getBetweenAmounts(@RequestParam double from, @RequestParam double to, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return bills.getByAmountRange(BigDecimal.valueOf(from), BigDecimal.valueOf(to), PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	@GetMapping("/bystatus")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Bill> getByStatus(@RequestParam String status, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		var s = statuses.getByStatus(status);
		return bills.getByStatus(s, PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	@GetMapping("/bycustomer")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Bill> getByCustomer(@RequestParam int customerId,@RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		var c = customers.getById(customerId);
		return bills.getByCustomer(c, PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	@GetMapping("/allannual")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<String> getAllAnnualAmount(@RequestParam int year, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return new PageImpl<String>(bills.getAllAmountsPerYear(year), PageRequest.of(pag, size, Sort.by(sort)), bills.getAllAmountsPerYear(year).size());
	}
	
	@GetMapping("/customertotal")
	@PreAuthorize("hasRole('ROLE_USER')")
	public BigDecimal getTotalPerCustomer(@RequestParam int customerId) {
		return bills.getTotalRevenue(customerId);
	}
	
	@GetMapping("/customeramount")
	@PreAuthorize("hasRole('ROLE_USER')")
	public BigDecimal getAmountByCustomerPerYear(@RequestParam int idCustomer, @RequestParam int year) {
		return bills.getBillingAmountPerYear(idCustomer, year);
	}
	
	@GetMapping("/all")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Bill> getAll(@RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return bills.getAll(PageRequest.of(pag, size, Sort.by(sort)));
	}
}
