package it.michelebello.epicenergyservice.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.michelebello.epicenergyservice.models.data.Type;
import it.michelebello.epicenergyservice.services.TypeService;

/*
 * In questa classe sono racchiusi gli end-point che riguardano
 * i tipi di società dei clienti del servizio
 */
@RestController
@RequestMapping("/types")
public class TypeController {

	@Autowired
	TypeService types;
	
	/*
	 * Il metodo per il salvataggio di un nuovo tipo
	 * data la semplicità della classe viene fatto direttamente
	 * con la stessa presa in ingresso
	 */
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Type save(@RequestBody Type type) {
		Type t = new Type();
		t.setType(type.getType());
		return types.save(t);
	}
	
	/*
	 * Discorso analogo al salvataggio vale per la modifica
	 */
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Type update(@RequestBody Type type) {
		return types.update(type);
	}
	
	/*
	 * Per l'eliminazione si ricorre invece ad un parametro che 
	 * rappresenta l'id dell'elemento da eliminare.
	 */
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Type delete(@RequestParam int id) {
		return types.delete(id);
	}
	
	/*
	 * L'unico elemento di recupero presente per ottenere
	 * l'elenco dei tipi di società.
	 */
	@GetMapping("/all")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Type> getAll(@RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return types.getAll(PageRequest.of(pag, size, Sort.by(sort)));
	}
	
}
