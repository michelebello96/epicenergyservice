package it.michelebello.epicenergyservice.controllers.restcontrollers;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.michelebello.epicenergyservice.controllers.models.LoginRequest;
import it.michelebello.epicenergyservice.controllers.models.LoginResponse;
import it.michelebello.epicenergyservice.controllers.models.RegistrationRequest;
import it.michelebello.epicenergyservice.models.data.Role;
import it.michelebello.epicenergyservice.models.data.User;
import it.michelebello.epicenergyservice.models.enums.RoleType;
import it.michelebello.epicenergyservice.security.JwtUtils;
import it.michelebello.epicenergyservice.security.service.UserDetailsImpl;
import it.michelebello.epicenergyservice.services.RoleService;
import it.michelebello.epicenergyservice.services.UserService;

/*
 * Questa classe presenta gli end-point di login e di registazione
 * al servizio.
 * Tali sono gli unici end-point sprovvisti di autenticazione.
 */

@RestController
@RequestMapping("")
public class AuthController {
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	RoleService roles;
	
	@Autowired
	UserService users;
	
	@Autowired
	JwtUtils jwtUtils;
	
	/*
	 * Questo metodo riceve le credenziali per l'autenticazione
	 * di un utente e in caso di riscontro positivo rimanda al client
	 * i dati necessari per poi interfacciarsi al resto dei servizi
	 */
	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) {
		
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		authentication.getAuthorities();
		
		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority()).collect(Collectors.toList());
		
		return new ResponseEntity<LoginResponse>(new LoginResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles, userDetails.getExpirationTime()),HttpStatus.OK);
	}
	
	/*
	 * Garantisce la creazione dell'utente nel DB valutando prima 
	 * l'unicità di alcuni campi.
	 * Tale metodo non restituisce il token necessario poi per
	 * Usare i servizi che verrà invece dato dal Login  
	 */
	@PostMapping("/register")
	public User registerNewUser(@RequestBody RegistrationRequest reg){
		
		User newUser = User.builder()
				.withEmail(reg.getEmail())
				.withName(reg.getName())
				.withPassword(reg.getPassword())
				.withSurname(reg.getSurname())
				.withUsername(reg.getUsername())
				.withRoles(new HashSet<Role>())
				.build();
		if(reg.isAdmin())
			newUser.getRoles().add(roles.getByRoleType(RoleType.ROLE_ADMIN));
		
		return users.save(newUser);
	}
}
