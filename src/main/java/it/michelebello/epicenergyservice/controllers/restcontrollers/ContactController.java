package it.michelebello.epicenergyservice.controllers.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.michelebello.epicenergyservice.controllers.models.ContactRequest;
import it.michelebello.epicenergyservice.models.data.Contact;
import it.michelebello.epicenergyservice.services.ContactService;
import it.michelebello.epicenergyservice.services.CustomerService;

/*
 * In questa classe sono racchiusi gli end-point che riguardano
 * i contatti dei clienti del servizio
 */
@RestController
@RequestMapping("/contacts")
public class ContactController {

	@Autowired
	ContactService contacts;
	@Autowired
	CustomerService customers;
	
	
	/*
	 * Il metodo per il salvataggio di un nuovo contatto,
	 * tramite il POJO ContactRequest va a comporre
	 * il contatto vero e proprio con tutti i collegamenti
	 * e le complessità annesse 
	 */
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Contact save(@RequestBody ContactRequest contact) {
		Contact c = Contact.builder()
				.withEmail(contact.getEmail())
				.withName(contact.getName())
				.withSurname(contact.getSurname())
				.withPhoneNumber(contact.getPhoneNumber())
				.withCustomer(customers.getById(contact.getIdCustomer()))
				.build();
					
		return contacts.save(c);			
	}
	
	/*
	 * Discorso analogo al salvataggio vale per la modifica
	 * in cui il POJO viene "analizzato" e si sviluppa il modello
	 * adatto per inserire poi la modifica in DB
	 */
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Contact update(@RequestBody ContactRequest contact) {
		Contact c = Contact.builder()
				.withEmail(contact.getEmail())
				.withName(contact.getName())
				.withSurname(contact.getSurname())
				.withPhoneNumber(contact.getPhoneNumber())
				.build();
		c.setId(contact.getId());
		if(contact.getIdCustomer()!=null)
			c.setCustomer(customers.getById(contact.getIdCustomer()));
		
		return contacts.update(c);
	}

	/*
	 * Per l'eliminazione si ricorre invece ad un parametro che 
	 * rappresenta l'id dell'elemento da eliminare.
	 * Tale elemento è facilmente reperibile tramite gli end-point 
	 * di recupero dati
	 */
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Contact delete(@RequestParam int id) {
		return contacts.delete(id);
	}
	
	/*
	 * Seguono vari metodi per il recupero degi contatti
	 * con vari vincoli
	 */
	@GetMapping("/byemail")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Contact getByEmail(@RequestParam String email) {
		return contacts.getByEmail(email);
	}
	
	@GetMapping("/byphone")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Contact getByPhoneNumber(@RequestParam String phoneNumber) {
		return contacts.getByPhoneNumber(phoneNumber);
	}
	
	@GetMapping("/byname")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Contact> getByName(@RequestParam String name, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort) {
		return contacts.getByName(name, PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	@GetMapping("/bysurname")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Contact> getBySurname(@RequestParam String surname, @RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return contacts.getBySurmane(surname, PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	@GetMapping("/bycustomer")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Contact> getByCustomer(@RequestParam int id,@RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return contacts.getByCustomer(customers.getById(id), PageRequest.of(pag, size, Sort.by(sort)));
	}
	
	@GetMapping("/all")
	@PreAuthorize("hasRole('ROLE_USER')")
	public Page<Contact> getAll(@RequestParam(defaultValue = "0") int pag, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "id") String sort){
		return contacts.getAll(PageRequest.of(pag, size, Sort.by(sort)));
	}
}