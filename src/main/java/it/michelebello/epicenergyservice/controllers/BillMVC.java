package it.michelebello.epicenergyservice.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.michelebello.epicenergyservice.controllers.models.BillRequest;
import it.michelebello.epicenergyservice.models.data.Bill;
import it.michelebello.epicenergyservice.repositories.BillingStatusRepository;
import it.michelebello.epicenergyservice.services.BillService;
import it.michelebello.epicenergyservice.services.CustomerService;

@Controller
@RequestMapping("/billmvc")
public class BillMVC {

	@Autowired
	BillService bills;
	@Autowired
	BillingStatusRepository status;
	@Autowired
	CustomerService customers;
	
	@GetMapping("/aggiungi")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView newBill(Model m) {
		m.addAttribute("bill", new BillRequest());
		m.addAttribute("newBill", 1);
		m.addAttribute("statuses", status.findAll());
		return new ModelAndView("newBill", m.asMap());
	}
	
	@GetMapping("/modifica")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView updateBill(Model m) {
		m.addAttribute("bill", new BillRequest());
		m.addAttribute("update", 1);
		m.addAttribute("statuses", status.findAll());
		return new ModelAndView("newBill", m.asMap());
	}
	
	@GetMapping("/rimuovi")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView deleteBill(Model m) {
		m.addAttribute("delete", 1);
		return new ModelAndView("newBill", m.asMap());
	}	
	
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String save(BillRequest bill) {
		var b = Bill.builder()
				.withBillingNumber(bill.getNumber())
				.withCustomer(customers.getById(bill.getCustomerId()))
				.build();
		if(bill.getYear()!=null && bill.getMonth()!=null && bill.getDay()!=null)
			b.setBillingDate(LocalDate.of(bill.getYear(), bill.getMonth(), bill.getDay()));
		if(bill.getAmount()!=null)
			b.setAmount(BigDecimal.valueOf(bill.getAmount()));
		if(bill.getStatus()!=null)
			b.setStatus(status.findByStatusIgnoreCase(bill.getStatus()).get());
		
		bills.save(b);
		return "home.html";
	}
	
	@PostMapping("/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String update(BillRequest bill) {
		var b = Bill.builder()
				.withBillingNumber(bill.getNumber())
				.build();
		if(bill.getYear()!=null && bill.getMonth()!=null && bill.getNumber()!=null)
			b.setBillingDate(LocalDate.of(bill.getYear(), bill.getMonth(), bill.getDay()));
		if(bill.getAmount()!=null)
			b.setAmount(BigDecimal.valueOf(bill.getAmount()));
		b.setId(bill.getId());
		if(bill.getCustomerId()!=null)
			b.setCustomer(customers.getById(bill.getCustomerId()));
		if(bill.getStatus()!=null)
			b.setStatus(status.findByStatusIgnoreCase(bill.getStatus()).get());
		
		bills.update(b);	
		return "home.html";
	}
	
	@PostMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String delete(@RequestParam Integer id) {
		bills.delete(id);
		return "home.html";
	}
}
