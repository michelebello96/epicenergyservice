package it.michelebello.epicenergyservice.models.enums;

//enum con i possibili utilizzi dell'edificio del cliente
public enum BuildingUse{
	REGISTERED_OFFICE, WORKPLACE
} 