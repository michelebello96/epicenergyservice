package it.michelebello.epicenergyservice.models.enums;

//enum con i ruoli possibili per gli utenti del servizio
public enum RoleType {
	ROLE_USER, ROLE_ADMIN
}
