package it.michelebello.epicenergyservice.models;

//Classe di comodo che estende l'entità base, con l'informazione sul momento della crezione

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@MappedSuperclass
public class WithCreationDate extends BaseEntity{

	@EqualsAndHashCode.Exclude
	@Column(insertable = false, updatable = false, columnDefinition = "timestamp default current_timestamp")
	private LocalDate creationDate;
}
