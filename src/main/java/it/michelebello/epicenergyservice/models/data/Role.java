package it.michelebello.epicenergyservice.models.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import it.michelebello.epicenergyservice.models.BaseEntity;
import it.michelebello.epicenergyservice.models.enums.RoleType;
import lombok.Data;
import lombok.EqualsAndHashCode;

//Questa classe rappresenta il ruolo di un utente del servizio

@Entity
@Data
@EqualsAndHashCode(callSuper = true )
public class Role extends BaseEntity{
	
	@Column(nullable = false, unique = true)
	@Enumerated(EnumType.STRING)
	public RoleType roleType;
	
}
