package it.michelebello.epicenergyservice.models.data;

import javax.persistence.Column;
import javax.persistence.Entity;

import it.michelebello.epicenergyservice.models.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

//Questa classe rappresenta lo stato di fatturazione

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class BillingStatus extends BaseEntity{

	@Column(nullable = false, unique = true)
	private String status;
}
