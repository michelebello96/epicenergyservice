package it.michelebello.epicenergyservice.models.data;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import it.michelebello.epicenergyservice.models.WithCreationDate;
import it.michelebello.epicenergyservice.security.StringAttributeConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//Questa classe rappresenta l'utente con i ruoli poi ad esso associati

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder(setterPrefix = "with")
@AllArgsConstructor
@NoArgsConstructor
public class User extends WithCreationDate{

	@Column(nullable = false, unique = true)
	private String username;
	@Column(nullable = false, unique = true)
	@Convert(converter = StringAttributeConverter.class)
	private String email;
	@Column(nullable = false)
	private String password;
	private String name;
	private String surname;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
			)
	private Set<Role> roles;

}
