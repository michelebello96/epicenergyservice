package it.michelebello.epicenergyservice.models.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import it.michelebello.epicenergyservice.citiesloader.models.City;
import it.michelebello.epicenergyservice.models.BaseEntity;
import it.michelebello.epicenergyservice.models.enums.BuildingUse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

// Questa classe rappresenta l'indirizzo di un cliente e ad essa è associata anche l'uso dell'edificio 
// Questa implementa l'interfaccia di confronto per poter poi ordinare gli indirizzi nei servizi
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Builder(setterPrefix = "with")
@AllArgsConstructor
@NoArgsConstructor
public class Address extends BaseEntity implements Comparable<Address>{

	@Column(nullable = false)
	private String street;
	@Column(nullable = false)
	private Integer houseNumber;
	private String locality;
	@Column(nullable = false)
	private String postalCode;
	@ManyToOne
	private City city;
	@Enumerated(EnumType.STRING)
	private BuildingUse use;
	@ManyToOne
	private Customer customer;
	

	@Override
	public int compareTo(Address o) {
		return this.city.getProvince().getAcronym().compareTo(o.city.getProvince().getAcronym());
	}

	
}
