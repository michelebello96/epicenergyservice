package it.michelebello.epicenergyservice.models.data;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import it.michelebello.epicenergyservice.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//Questa classe rappresenta la fattura del ciente e ad essa è associato anche lo stato della fattura

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder(setterPrefix = "with")
@AllArgsConstructor
@NoArgsConstructor
public class Bill extends BaseEntity{

	@Column(nullable = false)
	private LocalDate billingDate;
	@Column(nullable = false)
	private BigDecimal amount;  
	@Column(nullable = false)
	private Integer billingNumber;
	@ManyToOne
	private BillingStatus status;
	@ManyToOne
	private Customer customer;
	
}
