package it.michelebello.epicenergyservice.models.data;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import it.michelebello.epicenergyservice.models.WithCreationDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//Questa classe rappresenta il cliente attorno a cui ruotano poi direttamente o indirettamente tutti gli altri models

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder(setterPrefix = "with")
@AllArgsConstructor
@NoArgsConstructor
public class Customer extends WithCreationDate{

	@Column(nullable = false, unique = true)
	private String businessName;
	@Column(nullable = false, unique = true)
	private String vatNumber;
	@Column(nullable = false, unique = true)
	private String businessEmail;
	private LocalDate lastContact;
	@Column(nullable = false)
	private BigDecimal annualRevenue;
	@Column(nullable = false, unique = true)
	private String pec;
	@Column(unique = true)
	private String phoneNumber;
	@ManyToOne
	private Type type;
	
}
