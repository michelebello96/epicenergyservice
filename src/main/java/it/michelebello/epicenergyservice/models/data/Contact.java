package it.michelebello.epicenergyservice.models.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import it.michelebello.epicenergyservice.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

//Questa classe rappresenta un contatto del cliente

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder(setterPrefix = "with")
@NoArgsConstructor
@AllArgsConstructor
public class Contact extends BaseEntity{

	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private String surname;
	@Column(nullable = false, unique = true)
	private String email;
	@Column(unique = true)
	private String phoneNumber;
	@ManyToOne
	private Customer customer;
}
