package it.michelebello.epicenergyservice.models.data;

import javax.persistence.Column;
import javax.persistence.Entity;

import it.michelebello.epicenergyservice.models.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

//Questa classe rappresenta il tipo di società del cliente

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Type extends BaseEntity{

	@Column(nullable = false, unique = true)
	private String type; 
	
}
