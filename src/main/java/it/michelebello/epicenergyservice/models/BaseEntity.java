package it.michelebello.epicenergyservice.models;

//Classe di comodo che mette a fattor comune il campo id che sarà chiave primaria nel database (condiviso da tutte le classi "Entità")

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;

@Data
@MappedSuperclass
public class BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
}
