package it.michelebello.epicenergyservice.citiesloader.models;

import javax.persistence.Entity;

import it.michelebello.epicenergyservice.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Builder(setterPrefix = "with")
@AllArgsConstructor
@NoArgsConstructor
public class Province extends BaseEntity implements Comparable<Province>{

	private String name;
	private String acronym;

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Province o) {
		return this.acronym.compareTo(o.acronym);
	}

	
}
