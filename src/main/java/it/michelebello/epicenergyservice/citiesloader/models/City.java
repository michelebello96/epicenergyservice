package it.michelebello.epicenergyservice.citiesloader.models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import it.michelebello.epicenergyservice.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder(setterPrefix = "with")
@AllArgsConstructor
@NoArgsConstructor
public class City extends BaseEntity implements Comparable<City>{

	private String name;
	private boolean capital;
	private String fiscalCode;
	@ManyToOne
	private Province province;

	@Override
	public String toString() {
		return name + " (" + province.getAcronym() + ")";
	}

	@Override
	public int compareTo(City o) {
		if(this.province.compareTo(o.province) != 0)
			return this.province.compareTo(o.province);
		else
			return this.name.compareTo(o.name);
	}

}
