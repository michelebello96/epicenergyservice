package it.michelebello.epicenergyservice.citiesloader.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.citiesloader.models.City;
import it.michelebello.epicenergyservice.citiesloader.models.Province;

// Interfaccia dei servizi di città e province
public interface CitiesService {

	City add(City city);
	Optional<City> getByName(String name, String acr);
	Page<City> getByProvince(String acronym, Pageable pageable);
	Page<Province> getProvinces(Pageable pageable);
	Optional<City> getById(int id);
	List<City> getAll();
	
}
