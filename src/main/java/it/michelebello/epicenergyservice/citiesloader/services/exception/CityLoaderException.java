package it.michelebello.epicenergyservice.citiesloader.services.exception;

public class CityLoaderException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	/*
	 * Classe di errore molto semplice che può incorrere
	 * nella parte del servizio denominata cityloader
	 */
	public CityLoaderException() {	
	}
	
	public CityLoaderException(Throwable e) {
		super(e);
	}
}
