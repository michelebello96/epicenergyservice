package it.michelebello.epicenergyservice.citiesloader.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.michelebello.epicenergyservice.citiesloader.models.City;
import it.michelebello.epicenergyservice.citiesloader.models.Province;
import it.michelebello.epicenergyservice.citiesloader.repositories.CityRepository;
import it.michelebello.epicenergyservice.citiesloader.repositories.ProvinceRepository;
import it.michelebello.epicenergyservice.citiesloader.services.exception.CityLoaderException;

@Service
public class CitiesServiceImpl implements CitiesService{

	@Autowired
	CityRepository cities;
	
	@Autowired
	ProvinceRepository provinces;

	/*
	 * Tale metodo prende una città e la inserisce nel DB 
	 * facendo attenzione che la provincia di appartenenza
	 * non sia già stata registrata.
	 * In quel caso NON viene reinserita così da evitare 
	 * duplicati.
	 */
	@Override
	@Transactional 
	public City add(City city) {
		try {
			var province = provinces.findByAcronymLike(city.getProvince().getAcronym());
			Province p;
			if(province.isEmpty())
				p = provinces.save(city.getProvince());
			else
				p = province.get();
			city.setProvince(p);
			return cities.save(city);
		}catch(Exception e) {
			throw new CityLoaderException(e);
		}
	}
	
	/*
	 * Di seguito ci sono dei metodi di recupero delle città
	 * e delle province con vincoli vari,
	 * attingendo dal database con specifiche query 
	 * del repository
	 */

	@Override
	public Optional<City> getByName(String name, String acr) {
		try {
			return cities.findByNameIgnoreCaseAndProvinceAcronym(name, acr);
		}catch(Exception e) {
			throw new CityLoaderException(e);
		}
	}

	@Override
	public Page<City> getByProvince(String acronym, Pageable pageable) {
		try{
			return cities.findByProvinceAcronym(acronym, pageable);
		}catch(Exception e) {
			throw new CityLoaderException(e);
		}
	}

	@Override
	public Page<Province> getProvinces(Pageable pageable) {
		return provinces.findAll(pageable);
	}

	@Override
	public List<City> getAll() {
		return cities.findAll();
	}

	@Override
	public Optional<City> getById(int id) {
		return cities.findById(id);
	}

}
