package it.michelebello.epicenergyservice.citiesloader;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import it.michelebello.epicenergyservice.citiesloader.models.City;
import it.michelebello.epicenergyservice.citiesloader.models.Province;

public class CitiesLoader {
/*
 * Quest classe ci occupa di scomporre il file esterno contenente 
 * l'elenco delle citta componendo da questo i modelli di città e 
 * provincia che andranno a popolare il nostro database.
 */
	public static Set<City> load(InputStream is) {
		Set<City> cities = new HashSet<>();
		try (var scanner = new Scanner(is)) {
			for (var i = 0; i < 3; i++)
				scanner.nextLine();
			while (scanner.hasNext())
			{
				String line = scanner.nextLine();
				String[] parts = line.split(";");
				String cityName = parts[5];
				String cityCapital = parts[13];
				String cityFc = parts[19];
				String provinceName = parts[11];
				String provinceAcronym = parts[14];
			
				var province = new Province(provinceName, provinceAcronym);
				var city = City.builder()
						.withCapital(cityCapital.charAt(0) == '1')
						.withFiscalCode(cityFc)
						.withName(cityName)
						.withProvince(province)
						.build();

				cities.add(city);
			}
		} catch (Exception ex) {

		}
		return cities;
	}
}
