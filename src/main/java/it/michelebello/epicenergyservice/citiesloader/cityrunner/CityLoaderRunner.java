package it.michelebello.epicenergyservice.citiesloader.cityrunner;

import java.io.FileInputStream;

import javax.annotation.Priority;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import it.michelebello.epicenergyservice.citiesloader.CitiesLoader;
import it.michelebello.epicenergyservice.citiesloader.models.City;
import it.michelebello.epicenergyservice.citiesloader.models.Province;
import it.michelebello.epicenergyservice.citiesloader.services.CitiesService;

@Component
@Priority(2)
public class CityLoaderRunner implements CommandLineRunner{

	@Autowired
	CitiesService services;
	
	@Override
	public void run(String... args) throws Exception {
		/*
		 * Questo runner contiene i comandi che verranno eseguiti solo una volta 
		 * per popolare il DB con l'elenco delle citta e delle provincie
		 * presente su un file esterno.
		 * 
		 * Il codice carica le città e le province prese dal file tramite la classe CitiesLoader 
		 * e li inserisce in database.
		 * 
		 * NB. E stato posto nel CitiesService un filtro per evitare duplicati delle province.
		 */

//		var file = "C:\\Users\\Miky\\Desktop\\Epicode\\Progetti\\EpicEnergyService\\src\\main\\resources\\static\\Elenco-comuni-italiani.csv";
//		var cities = CitiesLoader.load(new FileInputStream(file));
//		
//		cities.stream().map(c -> City.builder()
//				.withCapital(c.isCapital())
//				.withFiscalCode(c.getFiscalCode())
//				.withName(c.getName())
//				.withProvince(Province.builder()
//						.withName(c.getProvince().getName())
//						.withAcronym(c.getProvince().getAcronym())
//						.build())
//				.build())
//		.forEach(c -> services.add(c));
		
	}
}
