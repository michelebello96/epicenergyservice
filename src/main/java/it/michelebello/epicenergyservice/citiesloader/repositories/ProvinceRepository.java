package it.michelebello.epicenergyservice.citiesloader.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import it.michelebello.epicenergyservice.citiesloader.models.Province;

@Repository
public interface ProvinceRepository extends PagingAndSortingRepository<Province, Integer> {

	Optional<Province> findByAcronymLike(String acronym);
	Page<Province> findAll(Pageable pageable);
}
