package it.michelebello.epicenergyservice.citiesloader.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import it.michelebello.epicenergyservice.citiesloader.models.City;

@Repository
public interface CityRepository extends PagingAndSortingRepository<City, Integer>{

	Optional<City> findByNameIgnoreCaseAndProvinceAcronym(String name, String acronym);
	List<City> findAll();
	Page<City> findByProvinceAcronym(String acronym, Pageable pageable);
}
