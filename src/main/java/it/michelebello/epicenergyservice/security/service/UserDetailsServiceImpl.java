package it.michelebello.epicenergyservice.security.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.michelebello.epicenergyservice.models.data.User;
import it.michelebello.epicenergyservice.repositories.UserRepository;

//Classe che gestisce l'autenticazione.

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository users;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> user = users.findByUsername(username);
		if (user.isPresent()) {
			return UserDetailsImpl.build(user.get());
		} else {
			throw new UsernameNotFoundException("Utente con username " + username + " non trovato!");
		}
	}

}
