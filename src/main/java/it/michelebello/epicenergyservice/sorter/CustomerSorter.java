package it.michelebello.epicenergyservice.sorter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.repositories.CustomerRepository;
import it.michelebello.epicenergyservice.sorter.errors.SortingError;

// Questa classe astratta crea la base per la catena di responsabilità usata le il sorting dei clienti
// Essa avrà un intero che indica l'indice di riferimento e l'anello successivo 
public abstract class CustomerSorter {

	protected int attribute;
	protected CustomerSorter next;
	
	public CustomerSorter() {
		
	};
	
	public CustomerSorter(int attribute, CustomerSorter next) {
		this.attribute = attribute;
		this.next = next;
	}

	public Page<Customer> trySort(int attribute, Pageable pageable, CustomerRepository customers){
		if(this.attribute == attribute)
			return this.getSorted(pageable, customers);
		else if(next != null)
			return next.trySort(attribute, pageable, customers);
		
		throw new SortingError();
	}
	
	abstract protected Page<Customer> getSorted(Pageable pageable,CustomerRepository customers);
}
