package it.michelebello.epicenergyservice.sorter.extensions;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import it.michelebello.epicenergyservice.models.data.Address;
import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.models.enums.BuildingUse;
import it.michelebello.epicenergyservice.repositories.AddressRepository;
import it.michelebello.epicenergyservice.repositories.CustomerRepository;
import it.michelebello.epicenergyservice.sorter.CustomerSorter;
import it.michelebello.epicenergyservice.sorter.errors.SortingError;

//quinto anello della catena di sorting con metodo per l'ordinamento alfabetico degli acronimi delle provincie delle sedi legali
public class SorterByProvince  extends CustomerSorter{

	public SorterByProvince(CustomerSorter c, AddressRepository add) {
		super(5,c);
		addresses = add;
	}

	private final AddressRepository addresses;

	@Override
	protected Page<Customer> getSorted(Pageable pageable,CustomerRepository customers) {
		try{
			List<Address> addr = addresses.findByUse(BuildingUse.REGISTERED_OFFICE, Sort.by("city.province"));
			Collections.sort(addr);
			List<Customer> cust = new LinkedList<>();
			addr.stream().forEach(a -> cust.add(a.getCustomer()));			
			return new PageImpl<Customer>(cust, pageable, cust.size());
		}catch(Exception e) {
			throw new SortingError();
		}
	}

}
