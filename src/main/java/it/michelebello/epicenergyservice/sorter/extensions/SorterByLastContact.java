package it.michelebello.epicenergyservice.sorter.extensions;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import it.michelebello.epicenergyservice.models.data.Customer;
import it.michelebello.epicenergyservice.repositories.CustomerRepository;
import it.michelebello.epicenergyservice.sorter.CustomerSorter;
import it.michelebello.epicenergyservice.sorter.errors.SortingError;

//quarto anello della catena di sorting con metodo di ordinamento per data dell' ultimo contatto con il cliente
public class SorterByLastContact extends CustomerSorter {

	public SorterByLastContact(CustomerSorter c) {
		super(4,c);
	}
	
	@Override
	protected Page<Customer> getSorted(Pageable pageable,CustomerRepository customers) {
		try{
			return customers.findAll(PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),Sort.by("lastContact")));
		}catch(Exception e) {
			throw new SortingError();
		}
	}
}
