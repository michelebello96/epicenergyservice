package it.michelebello.epicenergyservice.sorter.errors;

//Classe di errore che viene usato nel servizio di sorting degli utenti nel caso in sui si scelga un indice di sort non ancora implementato
public class SortingError extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public SortingError() {
		throw new RuntimeException("Sorting has failed!\n" + getLocalizedMessage());
	}
}
